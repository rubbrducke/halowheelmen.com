var win = window,
    docEl = document.documentElement,
    
	$header = document.getElementById('nav-primary');
	$mobileMenu = document.getElementById('hamburger-icon');
	
	$headerClass = $header.className;
	$mobileClass = $mobileMenu.className;
	
win.onscroll = function()
{
   var sTop = (this.pageYOffset || docEl.scrollTop)  - (docEl.clientTop || 0);
   
   if ( sTop > 200 )
   {
		$header.className = $headerClass + " solid";
		$mobileMenu.className = $mobileClass + "solid";
   }
   
   else
   {
		$header.className = $headerClass;
		$mobileMenu.className = $mobileClass;
   }
};