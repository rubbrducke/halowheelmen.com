<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * phpbb_url_matcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class phpbb_url_matcher extends Symfony\Component\Routing\Matcher\UrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);

        if (0 === strpos($pathinfo, '/mchat')) {
            // dmzx_mchat_page_custom_controller
            if ($pathinfo === '/mchat') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_dmzx_mchat_page_custom_controller;
                }

                return array (  '_controller' => 'dmzx.mchat.core:page_custom',  '_route' => 'dmzx_mchat_page_custom_controller',);
            }
            not_dmzx_mchat_page_custom_controller:

            // dmzx_mchat_page_archive_controller
            if ($pathinfo === '/mchat/archive') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_dmzx_mchat_page_archive_controller;
                }

                return array (  '_controller' => 'dmzx.mchat.core:page_archive',  '_route' => 'dmzx_mchat_page_archive_controller',);
            }
            not_dmzx_mchat_page_archive_controller:

            // dmzx_mchat_page_rules_controller
            if ($pathinfo === '/mchat/rules') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_dmzx_mchat_page_rules_controller;
                }

                return array (  '_controller' => 'dmzx.mchat.core:page_rules',  '_route' => 'dmzx_mchat_page_rules_controller',);
            }
            not_dmzx_mchat_page_rules_controller:

            // dmzx_mchat_page_whois_controller
            if (0 === strpos($pathinfo, '/mchat/whois') && preg_match('#^/mchat/whois/(?P<ip>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_dmzx_mchat_page_whois_controller;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'dmzx_mchat_page_whois_controller')), array (  '_controller' => 'dmzx.mchat.core:page_whois',));
            }
            not_dmzx_mchat_page_whois_controller:

            if (0 === strpos($pathinfo, '/mchat/action')) {
                // dmzx_mchat_action_add_controller
                if ($pathinfo === '/mchat/action/add') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_dmzx_mchat_action_add_controller;
                    }

                    return array (  '_controller' => 'dmzx.mchat.core:action_add',  '_route' => 'dmzx_mchat_action_add_controller',);
                }
                not_dmzx_mchat_action_add_controller:

                // dmzx_mchat_action_edit_controller
                if ($pathinfo === '/mchat/action/edit') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_dmzx_mchat_action_edit_controller;
                    }

                    return array (  '_controller' => 'dmzx.mchat.core:action_edit',  '_route' => 'dmzx_mchat_action_edit_controller',);
                }
                not_dmzx_mchat_action_edit_controller:

                // dmzx_mchat_action_del_controller
                if ($pathinfo === '/mchat/action/del') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_dmzx_mchat_action_del_controller;
                    }

                    return array (  '_controller' => 'dmzx.mchat.core:action_del',  '_route' => 'dmzx_mchat_action_del_controller',);
                }
                not_dmzx_mchat_action_del_controller:

                // dmzx_mchat_action_refresh_controller
                if ($pathinfo === '/mchat/action/refresh') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_dmzx_mchat_action_refresh_controller;
                    }

                    return array (  '_controller' => 'dmzx.mchat.core:action_refresh',  '_route' => 'dmzx_mchat_action_refresh_controller',);
                }
                not_dmzx_mchat_action_refresh_controller:

                // dmzx_mchat_action_whois_controller
                if ($pathinfo === '/mchat/action/whois') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_dmzx_mchat_action_whois_controller;
                    }

                    return array (  '_controller' => 'dmzx.mchat.core:action_whois',  '_route' => 'dmzx_mchat_action_whois_controller',);
                }
                not_dmzx_mchat_action_whois_controller:

            }

        }

        // hjw_calendar_controller
        if ($pathinfo === '/calendar/') {
            return array (  '_controller' => 'hjw.calendar.controller:display',  '_route' => 'hjw_calendar_controller',);
        }

        // phpbb_pages_main_controller
        if (0 === strpos($pathinfo, '/page') && preg_match('#^/page/(?P<route>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'phpbb_pages_main_controller')), array (  '_controller' => 'phpbb.pages.controller:display',));
        }

        if (0 === strpos($pathinfo, '/slickpanel')) {
            // slickpanel_home
            if ($pathinfo === '/slickpanel') {
                return array (  '_controller' => 'slickthemes.slickpanel.controller:full',  '_route' => 'slickpanel_home',);
            }

            // slickpanel_page
            if ($pathinfo === '/slickpanel/page') {
                return array (  '_controller' => 'slickthemes.slickpanel.controller:page',  '_route' => 'slickpanel_page',);
            }

            // slickpanel_userconfig
            if ($pathinfo === '/slickpanel/userconfig') {
                return array (  '_controller' => 'slickthemes.slickpanel.controller.userconfig:manage',  '_route' => 'slickpanel_userconfig',);
            }

        }

        // st_home
        if ($pathinfo === '/../') {
            return array('_route' => 'st_home');
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
