<?php

/* widgets/birthdays.html */
class __TwigTemplate_0387cf1aa0bcff87e47a3c2d7f1ef04feda97be04e8b7f925a4f1b5b35477b35 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (((isset($context["S_DISPLAY_BIRTHDAY_LIST"]) ? $context["S_DISPLAY_BIRTHDAY_LIST"] : null) && twig_length_filter($this->env, $this->getAttribute((isset($context["loops"]) ? $context["loops"] : null), "birthdays", array())))) {
            // line 2
            echo "  <div class=\"sidebar-widget birthday-list\">
\t<div class=\"widget-header\">
\t  <i class=\"widget-icon fi fi-cake\"></i>
\t  ";
            // line 5
            echo $this->env->getExtension('phpbb')->lang("BIRTHDAYS");
            echo "
\t  <span class=\"close\" data-toggle=\"collapse\" data-target=\"#widget-birthdayList\"></span>
\t</div>
\t<div class=\"widget-content collapse in\" id=\"widget-birthdayList\">
\t\t\t";
            // line 9
            // line 10
            echo "\t\t\t";
            if (twig_length_filter($this->env, $this->getAttribute((isset($context["loops"]) ? $context["loops"] : null), "birthdays", array()))) {
                echo $this->env->getExtension('phpbb')->lang("CONGRATULATIONS");
                echo $this->env->getExtension('phpbb')->lang("COLON");
                echo " <strong>";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["loops"]) ? $context["loops"] : null), "birthdays", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["birthdays"]) {
                    echo $this->getAttribute($context["birthdays"], "USERNAME", array());
                    if (($this->getAttribute($context["birthdays"], "AGE", array()) !== "")) {
                        echo " (";
                        echo $this->getAttribute($context["birthdays"], "AGE", array());
                        echo ")";
                    }
                    if ( !$this->getAttribute($context["birthdays"], "S_LAST_ROW", array())) {
                        echo ", ";
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['birthdays'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                echo "</strong>
\t\t\t";
            } else {
                // line 11
                echo "<span class=\"text-muted\">";
                echo $this->env->getExtension('phpbb')->lang("NO_BIRTHDAYS");
                echo "</span>";
            }
            // line 12
            echo "\t\t\t";
            // line 13
            echo "\t</div>
  </div>
";
        }
    }

    public function getTemplateName()
    {
        return "widgets/birthdays.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 13,  64 => 12,  59 => 11,  34 => 10,  33 => 9,  26 => 5,  21 => 2,  19 => 1,);
    }
}
