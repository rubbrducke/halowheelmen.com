<?php

/* ../../../ext/rmcgirr83/activity24hours/styles/prosilver/template/event/sidebar_stat_blocks.html */
class __TwigTemplate_0f485b70f7283c7741a922e5a5bdfb151af5936737eecb3d84110a34f70cd3a6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ( !(isset($context["S_IS_BOT"]) ? $context["S_IS_BOT"] : null)) {
            // line 2
            echo "<div class=\"sidebar-widget\">
\t<div class=\"widget-header\">
\t\t<i class=\"widget-icon fi fi-account-multiple\"></i>
\t\t<a href=\"\">";
            // line 5
            echo $this->env->getExtension('phpbb')->lang("TWENTYFOURHOUR_STATS");
            echo "</a>
\t</div>
\t\t\t\t<div class=\"stat-block 24stats\">
\t\t\t\t\t

\t\t\t\t\t<p>";
            // line 10
            echo (isset($context["HOUR_POSTS"]) ? $context["HOUR_POSTS"] : null);
            echo " &bull; ";
            echo (isset($context["HOUR_TOPICS"]) ? $context["HOUR_TOPICS"] : null);
            echo " <br> ";
            echo (isset($context["HOUR_USERS"]) ? $context["HOUR_USERS"] : null);
            echo "<br>

\t\t\t\t\t";
            // line 12
            echo (isset($context["USERS_24HOUR_TOTAL"]) ? $context["USERS_24HOUR_TOTAL"] : null);
            echo " ";
            echo (isset($context["GUEST_ONLINE_24"]) ? $context["GUEST_ONLINE_24"] : null);
            echo " ";
            echo $this->env->getExtension('phpbb')->lang("LAST_24_HOURS");
            echo $this->env->getExtension('phpbb')->lang("COLON");
            echo " ";
            if ( !(isset($context["USERS_ACTIVE"]) ? $context["USERS_ACTIVE"] : null)) {
                echo $this->env->getExtension('phpbb')->lang("NO_ONLINE_USERS");
            } else {
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["loops"]) ? $context["loops"] : null), "lastvisit", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["lastvisit"]) {
                    echo $this->getAttribute($context["lastvisit"], "USERNAME_FULL", array());
                    if ( !$this->getAttribute($context["lastvisit"], "S_LAST_ROW", array())) {
                        echo $this->env->getExtension('phpbb')->lang("COMMA_SEPARATOR");
                        echo " ";
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lastvisit'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
            }
            // line 13
            echo "
\t\t\t\t\t</p>
\t\t\t</div>
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "../../../ext/rmcgirr83/activity24hours/styles/prosilver/template/event/sidebar_stat_blocks.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 13,  43 => 12,  34 => 10,  26 => 5,  21 => 2,  19 => 1,);
    }
}
