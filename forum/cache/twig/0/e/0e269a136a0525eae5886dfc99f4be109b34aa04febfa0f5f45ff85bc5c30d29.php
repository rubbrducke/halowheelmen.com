<?php

/* ucp_avatar_options.html */
class __TwigTemplate_0e269a136a0525eae5886dfc99f4be109b34aa04febfa0f5f45ff85bc5c30d29 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "\t";
        if ( !(isset($context["S_AVATARS_ENABLED"]) ? $context["S_AVATARS_ENABLED"] : null)) {
            // line 2
            echo "\t\t<p class=\"st-panel\">";
            echo $this->env->getExtension('phpbb')->lang("AVATAR_FEATURES_DISABLED");
            echo "</p>
\t";
        }
        // line 4
        echo "
\t<fieldset class=\"st-form__fieldset\">
\t";
        // line 6
        if ((isset($context["ERROR"]) ? $context["ERROR"] : null)) {
            echo "<p class=\"error\">";
            echo (isset($context["ERROR"]) ? $context["ERROR"] : null);
            echo "</p>";
        }
        // line 7
        echo "\t\t<dl>
\t\t\t<dt><label>";
        // line 8
        echo $this->env->getExtension('phpbb')->lang("CURRENT_IMAGE");
        echo $this->env->getExtension('phpbb')->lang("COLON");
        echo "</label><br /><span>";
        echo $this->env->getExtension('phpbb')->lang("AVATAR_EXPLAIN");
        echo "</span></dt>
\t\t\t<dd><span class=\"avatar notRound\">";
        // line 9
        if ((isset($context["AVATAR"]) ? $context["AVATAR"] : null)) {
            echo (isset($context["AVATAR"]) ? $context["AVATAR"] : null);
        } else {
            echo $this->getAttribute((isset($context["definition"]) ? $context["definition"] : null), "NO_AVATAR", array());
        }
        echo "</span></dd>
\t\t</dl>
\t\t<dl>
\t\t\t<dt>&nbsp;</dt>
\t\t\t<dd><label for=\"avatar_delete\"><input type=\"checkbox\" name=\"avatar_delete\" id=\"avatar_delete\" /> ";
        // line 13
        echo $this->env->getExtension('phpbb')->lang("DELETE_AVATAR");
        echo "</label</dd>
\t\t</dl>
\t</fieldset>
\t<div class=\"st-section\">
\t\t<h3 class=\"st-section__header\">";
        // line 17
        echo $this->env->getExtension('phpbb')->lang("AVATAR_SELECT");
        echo "</h3>
\t\t<fieldset class=\"st-form__fieldset\">
\t\t\t<dl>
\t\t\t\t<dt><label>";
        // line 20
        echo $this->env->getExtension('phpbb')->lang("AVATAR_TYPE");
        echo $this->env->getExtension('phpbb')->lang("COLON");
        echo "</label></dt>
\t\t\t\t<dd><select name=\"avatar_driver\" id=\"avatar_driver\" data-togglable-settings=\"true\">
\t\t\t\t\t";
        // line 22
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["loops"]) ? $context["loops"] : null), "avatar_drivers", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["avatar_drivers"]) {
            // line 23
            echo "\t\t\t\t\t<option value=\"";
            echo $this->getAttribute($context["avatar_drivers"], "DRIVER", array());
            echo "\"";
            if ($this->getAttribute($context["avatar_drivers"], "SELECTED", array())) {
                echo " selected=\"selected\"";
            }
            echo " data-toggle-setting=\"#avatar_option_";
            echo $this->getAttribute($context["avatar_drivers"], "DRIVER", array());
            echo "\">";
            echo $this->getAttribute($context["avatar_drivers"], "L_TITLE", array());
            echo "</option>
\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['avatar_drivers'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "\t\t\t\t</select></dd>
\t\t\t</dl>
\t\t</fieldset>
\t\t<div id=\"avatar_options\">
\t";
        // line 29
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["loops"]) ? $context["loops"] : null), "avatar_drivers", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["avatar_drivers"]) {
            // line 30
            echo "\t\t<div id=\"avatar_option_";
            echo $this->getAttribute($context["avatar_drivers"], "DRIVER", array());
            echo "\">
\t\t<noscript>
\t\t<h3 class=\"avatar_section_header\">";
            // line 32
            echo $this->getAttribute($context["avatar_drivers"], "L_TITLE", array());
            echo "</h3>
\t\t</noscript>
\t\t<p class=\"st-panel\">";
            // line 34
            echo $this->getAttribute($context["avatar_drivers"], "L_EXPLAIN", array());
            echo "</p>
\t\t
\t
\t\t<fieldset class=\"st-form__fieldset\">
\t\t";
            // line 38
            echo $this->getAttribute($context["avatar_drivers"], "OUTPUT", array());
            echo "
\t\t</fieldset>
\t\t</div>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['avatar_drivers'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 42
        echo "\t\t</div>
\t</div>
";
        // line 44
        if ( !(isset($context["S_GROUP_MANAGE"]) ? $context["S_GROUP_MANAGE"] : null)) {
            // line 45
            echo "\t<fieldset class=\"st-form__submit\">
\t\t<button type=\"reset\" value=\"";
            // line 46
            echo $this->env->getExtension('phpbb')->lang("RESET");
            echo "\" name=\"reset\" class=\"button2\"><i class=\"fi fi-reload\"></i> ";
            echo $this->env->getExtension('phpbb')->lang("RESET");
            echo "</button>
\t\t&nbsp;
\t\t<button type=\"submit\" name=\"submit\" value=\"";
            // line 48
            echo $this->env->getExtension('phpbb')->lang("SUBMIT");
            echo "\" class=\"button1\">";
            echo $this->env->getExtension('phpbb')->lang("SUBMIT");
            echo " <i class=\"fi fi-arrow-";
            echo (isset($context["S_CONTENT_FLOW_END"]) ? $context["S_CONTENT_FLOW_END"] : null);
            echo "\"></i></button>
\t</fieldset>
";
        }
    }

    public function getTemplateName()
    {
        return "ucp_avatar_options.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  153 => 48,  146 => 46,  143 => 45,  141 => 44,  137 => 42,  127 => 38,  120 => 34,  115 => 32,  109 => 30,  105 => 29,  99 => 25,  82 => 23,  78 => 22,  72 => 20,  66 => 17,  59 => 13,  48 => 9,  41 => 8,  38 => 7,  32 => 6,  28 => 4,  22 => 2,  19 => 1,);
    }
}
