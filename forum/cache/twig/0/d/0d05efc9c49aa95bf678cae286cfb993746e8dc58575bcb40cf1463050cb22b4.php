<?php

/* ucp_pm_message_header.html */
class __TwigTemplate_0d05efc9c49aa95bf678cae286cfb993746e8dc58575bcb40cf1463050cb22b4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<div class=\"ucp-section__header\">
\t\t<h2 class=\"ucp-section__title\">";
        // line 3
        echo $this->env->getExtension('phpbb')->lang("TITLE");
        if ((isset($context["CUR_FOLDER_NAME"]) ? $context["CUR_FOLDER_NAME"] : null)) {
            echo $this->env->getExtension('phpbb')->lang("COLON");
            echo " ";
            echo (isset($context["CUR_FOLDER_NAME"]) ? $context["CUR_FOLDER_NAME"] : null);
        }
        echo "</h2>
\t\t";
        // line 4
        if ((isset($context["S_VIEW_MESSAGE"]) ? $context["S_VIEW_MESSAGE"] : null)) {
            // line 5
            echo "\t\t<div class=\"ucp-section__headerlinks\">
\t\t\t<a class=\"arrow-";
            // line 6
            echo (isset($context["S_CONTENT_FLOW_BEGIN"]) ? $context["S_CONTENT_FLOW_BEGIN"] : null);
            echo "\" href=\"";
            echo (isset($context["U_CURRENT_FOLDER"]) ? $context["U_CURRENT_FOLDER"] : null);
            echo "\">";
            echo $this->env->getExtension('phpbb')->lang("RETURN_TO_FOLDER");
            echo "</a>
\t\t</div>
\t\t";
        }
        // line 9
        echo "\t</div>

<form id=\"viewfolder\" method=\"post\" action=\"";
        // line 11
        echo (isset($context["S_PM_ACTION"]) ? $context["S_PM_ACTION"] : null);
        echo "\">

<div class=\"panel\">
\t<div class=\"inner\">
\t";
        // line 15
        if (((isset($context["FOLDER_STATUS"]) ? $context["FOLDER_STATUS"] : null) && ((isset($context["FOLDER_MAX_MESSAGES"]) ? $context["FOLDER_MAX_MESSAGES"] : null) != 0))) {
            echo "<p>";
            echo (isset($context["FOLDER_STATUS"]) ? $context["FOLDER_STATUS"] : null);
            echo "</p>";
        }
        // line 16
        echo "
\t<div class=\"action-bar top\">
\t";
        // line 18
        if ((((isset($context["U_POST_REPLY_PM"]) ? $context["U_POST_REPLY_PM"] : null) || (isset($context["U_POST_NEW_TOPIC"]) ? $context["U_POST_NEW_TOPIC"] : null)) || (isset($context["U_FORWARD_PM"]) ? $context["U_FORWARD_PM"] : null))) {
            // line 19
            echo "\t\t<div class=\"buttons\">
\t\t\t";
            // line 20
            if ((isset($context["U_POST_REPLY_PM"]) ? $context["U_POST_REPLY_PM"] : null)) {
                // line 21
                echo "\t\t\t\t<a title=\"";
                echo $this->env->getExtension('phpbb')->lang("POST_REPLY_PM");
                echo "\" href=\"";
                echo (isset($context["U_POST_REPLY_PM"]) ? $context["U_POST_REPLY_PM"] : null);
                echo "\" class=\"btn btn-success\">
\t\t\t\t\t<i class=\"fi fi-reply\"></i> ";
                // line 22
                echo $this->env->getExtension('phpbb')->lang("BUTTON_PM_REPLY");
                echo "
\t\t\t\t</a>
\t\t\t";
            } elseif (            // line 24
(isset($context["U_POST_NEW_TOPIC"]) ? $context["U_POST_NEW_TOPIC"] : null)) {
                // line 25
                echo "\t\t\t\t<a href=\"";
                echo (isset($context["U_POST_NEW_TOPIC"]) ? $context["U_POST_NEW_TOPIC"] : null);
                echo "\" accesskey=\"n\" title=\"";
                echo $this->env->getExtension('phpbb')->lang("UCP_PM_COMPOSE");
                echo "\" class=\"btn btn-success btn-createnew\">
\t\t\t\t\t<i class=\"fi fi-plus\"></i> ";
                // line 26
                echo $this->env->getExtension('phpbb')->lang("BUTTON_PM_NEW");
                echo "
\t\t\t\t</a>
\t\t\t";
            }
            // line 29
            echo "\t\t\t";
            if ((isset($context["U_FORWARD_PM"]) ? $context["U_FORWARD_PM"] : null)) {
                // line 30
                echo "\t\t\t\t<a title=\"";
                echo $this->env->getExtension('phpbb')->lang("POST_FORWARD_PM");
                echo "\" href=\"";
                echo (isset($context["U_FORWARD_PM"]) ? $context["U_FORWARD_PM"] : null);
                echo "\" class=\"btn btn-default\">
\t\t\t\t\t";
                // line 31
                echo $this->env->getExtension('phpbb')->lang("BUTTON_PM_FORWARD");
                echo " <i class=\"fi fi-arrow-";
                echo (isset($context["S_CONTENT_FLOW_END"]) ? $context["S_CONTENT_FLOW_END"] : null);
                echo "\"></i>
\t\t\t\t</a>
\t\t\t";
            }
            // line 34
            echo "\t\t\t";
            if (((isset($context["U_POST_REPLY_PM"]) ? $context["U_POST_REPLY_PM"] : null) && ((isset($context["S_PM_RECIPIENTS"]) ? $context["S_PM_RECIPIENTS"] : null) > 1))) {
                // line 35
                echo "\t\t\t\t<a title=\"";
                echo $this->env->getExtension('phpbb')->lang("REPLY_TO_ALL");
                echo "\" href=\"";
                echo (isset($context["U_POST_REPLY_ALL"]) ? $context["U_POST_REPLY_ALL"] : null);
                echo "\" class=\"btn btn-default\">
\t\t\t\t\t<i class=\"fi fi-reply-all\"></i> ";
                // line 36
                echo $this->env->getExtension('phpbb')->lang("BUTTON_PM_REPLY_ALL");
                echo "
\t\t\t\t</a>
\t\t\t";
            }
            // line 39
            echo "\t\t</div>
\t";
        }
        // line 41
        echo "
\t";
        // line 42
        if (( !(isset($context["S_IS_BOT"]) ? $context["S_IS_BOT"] : null) && (isset($context["U_PRINT_PM"]) ? $context["U_PRINT_PM"] : null))) {
            // line 43
            echo "\t\t<div class=\"dropdown-container dropdown-button-control topic-tools\">
\t\t\t<span title=\"";
            // line 44
            echo $this->env->getExtension('phpbb')->lang("PM_TOOLS");
            echo "\" class=\"dropdown-trigger dropdown-select button icon-button tools-icon\"></span>
\t\t\t<div class=\"dropdown hidden\">
\t\t\t\t<div class=\"pointer\"><div class=\"pointer-inner\"></div></div>
\t\t\t\t<ul class=\"dropdown-contents\">
\t\t\t\t\t";
            // line 48
            if ((isset($context["U_PRINT_PM"]) ? $context["U_PRINT_PM"] : null)) {
                echo "<li class=\"small-icon icon-print\"><a href=\"";
                echo (isset($context["U_PRINT_PM"]) ? $context["U_PRINT_PM"] : null);
                echo "\" title=\"";
                echo $this->env->getExtension('phpbb')->lang("PRINT_PM");
                echo "\" accesskey=\"p\">";
                echo $this->env->getExtension('phpbb')->lang("PRINT_PM");
                echo "</a></li>";
            }
            // line 49
            echo "\t\t\t\t</ul>
\t\t\t</div>
\t\t</div>
\t";
        }
        // line 53
        echo "
\t";
        // line 54
        if (((isset($context["TOTAL_MESSAGES"]) ? $context["TOTAL_MESSAGES"] : null) || (isset($context["S_VIEW_MESSAGE"]) ? $context["S_VIEW_MESSAGE"] : null))) {
            // line 55
            echo "\t\t<div class=\"right-side\">
\t\t\t";
            // line 56
            if ((isset($context["U_MARK_ALL"]) ? $context["U_MARK_ALL"] : null)) {
                echo "<a href=\"";
                echo (isset($context["U_MARK_ALL"]) ? $context["U_MARK_ALL"] : null);
                echo "\" class=\"btn btn-default\"><i class=\"fi fi-check-all\"></i> ";
                echo $this->env->getExtension('phpbb')->lang("PM_MARK_ALL_READ");
                echo "</a>";
            }
            // line 57
            echo "\t\t\t";
            if (( !(isset($context["S_VIEW_MESSAGE"]) ? $context["S_VIEW_MESSAGE"] : null) && ((isset($context["FOLDER_CUR_MESSAGES"]) ? $context["FOLDER_CUR_MESSAGES"] : null) != 0))) {
                // line 58
                echo "\t\t\t\t<!--";
                echo (isset($context["TOTAL_MESSAGES"]) ? $context["TOTAL_MESSAGES"] : null);
                echo "-->
\t\t\t\t";
                // line 59
                $location = "pagination.html";
                $namespace = false;
                if (strpos($location, '@') === 0) {
                    $namespace = substr($location, 1, strpos($location, '/') - 1);
                    $previous_look_up_order = $this->env->getNamespaceLookUpOrder();
                    $this->env->setNamespaceLookUpOrder(array($namespace, '__main__'));
                }
                $this->loadTemplate("pagination.html", "ucp_pm_message_header.html", 59)->display($context);
                if ($namespace) {
                    $this->env->setNamespaceLookUpOrder($previous_look_up_order);
                }
                // line 60
                echo "\t\t\t";
            }
            // line 61
            echo "\t\t</div>
\t";
        }
        // line 63
        echo "\t</div>
";
    }

    public function getTemplateName()
    {
        return "ucp_pm_message_header.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  214 => 63,  210 => 61,  207 => 60,  195 => 59,  190 => 58,  187 => 57,  179 => 56,  176 => 55,  174 => 54,  171 => 53,  165 => 49,  155 => 48,  148 => 44,  145 => 43,  143 => 42,  140 => 41,  136 => 39,  130 => 36,  123 => 35,  120 => 34,  112 => 31,  105 => 30,  102 => 29,  96 => 26,  89 => 25,  87 => 24,  82 => 22,  75 => 21,  73 => 20,  70 => 19,  68 => 18,  64 => 16,  58 => 15,  51 => 11,  47 => 9,  37 => 6,  34 => 5,  32 => 4,  23 => 3,  19 => 1,);
    }
}
