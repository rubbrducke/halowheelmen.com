<?php

/* @dmzx_mchat/event/index_body_markforums_before.html */
class __TwigTemplate_0d50fd2eacae5b007ec082941fa4d15fbb1c714e875301d3b528e3a2706c946d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (((isset($context["MCHAT_INDEX"]) ? $context["MCHAT_INDEX"] : null) && (isset($context["MCHAT_LOCATION"]) ? $context["MCHAT_LOCATION"] : null))) {
            $location = "mchat_body.html";
            $namespace = false;
            if (strpos($location, '@') === 0) {
                $namespace = substr($location, 1, strpos($location, '/') - 1);
                $previous_look_up_order = $this->env->getNamespaceLookUpOrder();
                $this->env->setNamespaceLookUpOrder(array($namespace, '__main__'));
            }
            $this->loadTemplate("mchat_body.html", "@dmzx_mchat/event/index_body_markforums_before.html", 1)->display($context);
            if ($namespace) {
                $this->env->setNamespaceLookUpOrder($previous_look_up_order);
            }
        }
    }

    public function getTemplateName()
    {
        return "@dmzx_mchat/event/index_body_markforums_before.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
