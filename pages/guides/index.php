
<!----------------------------------------
	START OF PAGE
----------------------------------------->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

	<?php $includes = $_SERVER['DOCUMENT_ROOT']."/site/pages/page-appendages/"; ?>
	<?php include( $includes."header.php" ); ?>

</head>


<!----------------------------------------
	BODY
----------------------------------------->

<body>


	<!----------------------------------------
		NAVIGATION
	----------------------------------------->
	
	<?php include( $includes."mobile-navigation.php" ); ?>
	
	<div class="site-wrapper">
	
	<?php include( $includes."navigation.php"); ?>
	
	
	<!----------------------------------------
		HERO IMAGE / VIDEO
	----------------------------------------->
	
	<?php heroImage( "Guides", "/site/images/featured2.jpg", "" ); ?>
	
	
	<!---------------------------------------- 
		PAGE CONTENT
	----------------------------------------->
	
	<div id="content" class="section-none">
	
		<div class="gallery horizontal large">
		
			<div>
			<a href="/site/pages/guides/halo-5/h5-guide.php">
				<div class="img-container tall">
				
					<div class="guide-preview">
						<div class="article-title">Halo 5</div>
						<div class="article-info">Warzone !!</div>
					</div>
					
					<img class="article-image" src="/site/images/index/homepage2.jpg" alt>
				
				</div>
			</a>
			</div>

			<div>
			<a href="">
				<div class="img-container tall">
				
					<div class="guide-preview">
						<div class="article-title">Halo Reach</div>
						<div class="article-info">Invasion !!!</div>
					</div>
					
					<img class="article-image" src="/site/images/index/homepage1.jpg" alt>
				
				</div>
			</a>
			</div>
		
			<div>
			<a href="">
				<div class="img-container tall">
				
					<div class="guide-preview">
						<div class="article-title">Halo 3</div>
						<div class="article-info">Ye olden days</div>
					</div>
					
					<img class="article-image" src="/site/images/index/homepage3.jpg" alt>
				
				</div>
			</a>
			</div>
		
		</div>
	
	</div>
	
	
	<!---------------------------------------- 
		FOOTER
	----------------------------------------->
	
	<?php include( $includes."footer.php" ); ?>
	
	</div>
	
</body>
</html>
