
<!----------------------------------------
	HERE BE THE BEGINNINGS OF THE PAGE 
	FOR OUR VIEWING
----------------------------------------->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>


	<!----------------------------------------
		INCLUDES
	----------------------------------------->
	
	<?php $pages = $_SERVER['DOCUMENT_ROOT']."/site/pages/"; ?>
	<?php $includes = $_SERVER['DOCUMENT_ROOT']."/site/pages/page-appendages/"; ?>
	
	<?php include( $includes."forum-ids.php" ); ?>
	<?php include( $includes."header.php" ); ?>
	<?php include( $includes."database-login.php" ); ?>
	<?php include( $includes."user-session.php" ); ?>

</head>


<!----------------------------------------
	BODY
----------------------------------------->

<body>

	<!-- Mobile Nav -->
	
	<?php include( $includes."mobile-navigation.php" ); ?>
	
	
	<!----------------------------------------
		SITE WRAPPER
	----------------------------------------->
	
	<div class="site-wrapper">
	
	
	<!-- Standard Nav -->
	
	<?php include( $includes."navigation.php"); ?>
	
	
	<!-- Banner -->
	
	<?php heroImage( "Halo 5 Guides", "/site/images/featured2.jpg", "" ); ?>

	
	<!---------------------------------------- 
		PAGE CONTENT
	----------------------------------------->
	
	<div id="content" class="section-none gallery horizontal">
	
		<div>
		<h1>Maps and Gametypes</h1>
		<div class="gallery medium horizontal-wrap">
			<?php include( $pages."guides/halo-5/h5-map-guides.php" ); ?>
		</div>
		</div>
		
		<div>
		<h1>Vehicles and Weapons</h1>
		<div class="gallery medium horizontal-wrap">
			<?php include( $pages."guides/halo-5/h5-vehicle-guides.php" ); ?>
		</div>
		</div>
		
	</div>
	
	<!---------------------------------------- 
		FOOTER
	----------------------------------------->
	
	<?php include( $includes."dynamic-gallery" ); ?>
	<?php include( $includes."footer.php" ); ?>
	
	</div>
	
</body>
</html>
