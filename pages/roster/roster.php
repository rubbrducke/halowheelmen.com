
<?php

$staffSQL = "SELECT distinct user_avatar, user_avatar_width,  user_avatar_height, username, pf_admin_rank, pf_user_gamertag, pf_admin_bio, rank_image, user_colour
FROM phpbb3_user_group ug
INNER JOIN phpbb3_users u ON ug.user_id = u.user_id
INNER JOIN phpbb3_profile_fields_data fd on u.user_id = fd.user_id
inner join phpbb3_ranks r on u.user_rank = r.rank_id
WHERE ug.group_id =76 or ug.group_id = 110
order by u.user_id";

$wheelmenSQL = "SELECT distinct user_avatar, user_avatar_width, user_avatar_height, username, pf_user_gamertag
FROM phpbb3_user_group ug
INNER JOIN phpbb3_users u ON ug.user_id = u.user_id
INNER JOIN phpbb3_profile_fields_data fd on u.user_id = fd.user_id
WHERE ug.group_id = 109 and ug.user_pending = 0
order by u.user_id";

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>


	<!----------------------------------------
		PHP VARIABLES
	----------------------------------------->

	<?php $includes = $_SERVER['DOCUMENT_ROOT']."/site/pages/page-appendages/"; ?>
	
	
	<!----------------------------------------
		HEADER
	----------------------------------------->
	
	<?php include( $includes."database-login.php" ); ?>
	<?php include( $includes."header.php" ); ?>
	
</head>


<!----------------------------------------
	BODY
----------------------------------------->

<body>


	<!----------------------------------------
		NAVIGATION
	----------------------------------------->
	
	<?php include( $includes."/mobile-navigation.php" ); ?>
	
	
	<!-- Site Wrapper -->
	
	<div class="site-wrapper">
	
	
	<!-- Standard Navigation -->
	
	<?php include( $includes."navigation.php"); ?>
	

	<!----------------------------------------
		BANNER
	----------------------------------------->
	
	<?php heroImage( "Roster", "/site/images/featured2.jpg", "" ); ?>
	
	<!---------------------------------------- 
		PAGE CONTENT
	----------------------------------------->
	
	<div id="content" class="section-none bg-blue">
	
	
		<!-- Staff Gallery -->
	
		<h1>Staff</h1>
		<div class="gallery small horizontal-wrap">
	
	
		<!-- Beginning of Staff loop -->
		
		<?php
			$result = mysql_query($staffSQL);
			for($i=0;$i<mysql_num_rows($result);$i++)
			{
				$row = mysql_fetch_array($result);
				$avatar = $row["user_avatar"];
				$aw = $row["user_avatar_width"];
				$ah = $row["user_avatar_height"];
				$name = $row["username"];
				$adminRank = stripslashes($row["pf_admin_rank"]);
				$gamertag = $row["pf_user_gamertag"];
				$color = $row["user_colour"];
        ?>
		
			<div class="section-small">
				<div class="img-container tall">
					<div class="profile">
						<h5><?=$gamertag?></h6>
						<h6><?=$adminRank?></h6>
					</div>
					<img src=<?=$avatar?> />
				</div>
			</div>
		
		
		<!-- End of staff loop -->
		
		<?php
		}
        ?>
		
		</div>


		<!-- Wheelmen Gallery -->
		
		<h1>Wheelmen</h1>
		<div class="gallery small horizontal-wrap">
	
	
		<!-- Beginning of Staff loop -->
		
		<?php
			$result = mysql_query($wheelmenSQL);
			
			for($i=0;$i<mysql_num_rows($result);$i++)
			{
				$row = mysql_fetch_array($result);
				$avatar = $row["user_avatar"];
				$aw = $row["user_avatar_width"];
				$ah = $row["user_avatar_height"];
				$name = $row["username"];
				$gamertag = $row["pf_user_gamertag"];
        ?>
		
			<div class="section-small">
				<div class="img-container tall">
					<div class="profile">
						<h6><?=$gamertag?></h6>
					</div>
					<img src=<?=$avatar?> />
				</div>
				
				
			</div>
		
		
		<!-- End of staff loop -->
		
		<?php
		}
        ?>
		
		
		</div>

		
	</div>
	
	
	<!----------------------------------------
		FOOTER
	----------------------------------------->
	
	<?php include( $includes."dynamic-gallery" ); ?>
	<?php include( $includes."footer.php"); ?>
	
	
</body>
</html>