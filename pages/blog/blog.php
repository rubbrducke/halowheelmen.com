
<!----------------------------------------
	START OF PAGE
----------------------------------------->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

	<?php $includes = $_SERVER['DOCUMENT_ROOT']."/site/pages/page-appendages/"; ?>
	<?php $pages = $_SERVER['DOCUMENT_ROOT']."/site/pages/"; ?>
	
	<?php include( $includes."header.php" ); ?>
	<?php include( $includes."database-login.php" ); ?>
	<?php include( $includes."forum-ids.php" ); ?>
	<?php include( $includes."user-session.php" ); ?>

</head>


<!----------------------------------------
	BODY
----------------------------------------->

<body>


	<!----------------------------------------
		NAVIGATION
	----------------------------------------->
	
	<?php include( $includes."mobile-navigation.php" ); ?>
	
	<div class="site-wrapper">
	
	<?php include( $includes."navigation.php"); ?>
	
	
	<!----------------------------------------
		HERO IMAGE / VIDEO
	----------------------------------------->
	
	<?php heroImage( "Blog", "/site/images/featured2.jpg", "" ); ?>
	
	
	<!---------------------------------------- 
		PAGE CONTENT
	----------------------------------------->
	
	<div id="content" class="section-none">

		<?php include( $pages."blog/blog-post-puller.php" ); ?>
	
	</div>
	
	
	<!---------------------------------------- 
		FOOTER
	----------------------------------------->
	
	<?php include( $includes."footer.php" ); ?>
	
	</div>
	
</body>
</html>
