<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

	<!----------------------------------------
		PHP VARIABLES
	----------------------------------------->

	<?php $id = $_GET[ 'id' ];
	$id = filter_var( $id, FILTER_VALIDATE_INT ); ?>
	<?php $includes = $_SERVER['DOCUMENT_ROOT']."/site/pages/page-appendages/"; ?>
	
	
	<!----------------------------------------
		HEADER
	----------------------------------------->
	
	<?php include( $includes."forum-ids.php" ); ?>
	<?php include( $includes."user-session.php" ); ?>
	<?php include( $includes."header.php" ); ?>
	
</head>


<!----------------------------------------
	BODY
----------------------------------------->

<body>


	<!----------------------------------------
		NAVIGATION
	----------------------------------------->
	
	<?php include( $includes."/mobile-navigation.php" ); ?>
	
	
	<!-- Site Wrapper -->
	
	<div class="site-wrapper">
	
	
	<!-- Standard Navigation -->
	
	<?php include( $includes."navigation.php"); ?>
	
	
	<!---------------------------------------- 
		PAGE CONTENT
	----------------------------------------->
	
	<div id="content" class="section-none bg-blue">
	
		<?php if ( filter_var( $id, FILTER_VALIDATE_INT ) )
		{
			include( $includes."article-syndication.php" ); 
		}
		else
		{
			echo "<h1>Error. Don't be rude, pls</h1>";
		}?>
	</div>
	
	
	<!----------------------------------------
		FOOTER
	----------------------------------------->
	
	<?php include( $includes."footer.php"); ?>
	
	
</body>
</html>