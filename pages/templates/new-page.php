
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

	<?php $includes = $_SERVER['DOCUMENT_ROOT']."/site/pages/page-appendages/"; ?>
	<?php include( $includes."header.php" ); ?>
	
</head>


<!----------------------------------------
	BODY
----------------------------------------->

<body>


	<!----------------------------------------
		BANNER
	----------------------------------------->
	
	<?php include( $includes."navigation.php"); ?>
	
	
	<!---------------------------------------- 
		PAGE CONTENT
	----------------------------------------->
	
	<div id="content" class="section-none">
	
		<!-- Content goes here. -->
	
	</div>
	
	
	<!----------------------------------------
		FOOTER
	----------------------------------------->
	
	<?php include( $includes."footer.php"); ?>
	
	
</body>
</html>