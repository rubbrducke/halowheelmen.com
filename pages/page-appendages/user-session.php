<?php

define('PHPBB_ROOT_PATH', $_SERVER['DOCUMENT_ROOT'].'/site/forum/');
define('IN_PHPBB', true);
$phpbb_root_path = PHPBB_ROOT_PATH;
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);
include($phpbb_root_path . 'includes/functions_display.' . $phpEx);
include($phpbb_root_path . 'includes/bbcode.' . $phpEx);

// Start session management
$user->session_begin(false);
$auth->acl($user->data);

// Grab user preferences
$user->setup();

?>