
<!---------------------------------------- 
	MOBILE NAVIGATION 
----------------------------------------->

<input type="checkbox" id="mobile-menu-trigger" />
<label id="hamburger-icon" for="mobile-menu-trigger">
	<?php echo file_get_contents( $includes."images/hamburger-menu.svg" ); ?>
</label>

<div class="menu-hamburger">
	<?php include( $includes."navigation-links.html"); ?>
</div>