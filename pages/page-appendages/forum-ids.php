<?php

	/* ----------------------------------------
		FORUM ID's
		
		These are the forums that are pulled
		from by the post pullers.
	----------------------------------------- */
	
	define( 'FORUM_H5_MAPS', 		160 );
	define( 'FORUM_H5_VEHICLES', 	161 );
	define( 'FORUM_HR_MAPS', 		164 );
	define( 'FORUM_HR_VEHICLES', 	163 );
	define( 'FORUM_H3_MAPS', 		165 );
	define( 'FORUM_H3_VEHICLES', 	166 );
	define( 'FORUM_NEWS', 			154 );
	define( 'FORUM_MOTM', 			172 );
?>