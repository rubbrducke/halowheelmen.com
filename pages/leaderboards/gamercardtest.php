<?php

// Settings
$gamertag = 'I%20R%20Zorn';
$profileUrl = 'http://api.xboxleaders.com/v2/?format=json&gamertag='.$gamertag;

// Get information about me
$info = file_get_contents($profileUrl);

// To JSON
$json = json_decode($info);
$user = $json->user;

?>


<style>
	
	.gamercard {
		border: 1px solid #bdbec1;
		padding: 10px;
		width: 600px;
		font-family: arial, sans-serif;
		font-size: 12px;
		color: #bdbec1;
		
		background-image: -webkit-linear-gradient(#ddd, #fff, #e9fdce);
		background-image: -moz-linear-gradient(top, #ddd, #fff, #e9fdce);
		background-image: -ms-linear-gradient(#ddd, #fff, #e9fdce);
		background-image: -o-linear-gradient(#ddd, #fff, #e9fdce);
		background-image: linear-gradient(#ddd, #fff, #e9fdce);
	}
	
	.gamercard img {
		display: block;
	}
	
	.gamercard .avatar {
		float: right;
		width: 150px;
		height: 300px;
		margin: -60px 0 0 50px;
	}
	
	.gamercard h1 {
		font-weight: normal;
	}
		
		.gamercard h1 img {
			display: inline-block; 
			padding-right: 10px;
			width: 24px; 
			height: 24px;
		}
		
		.gamercard h1 a {
			text-decoration: none;
		}
		
			.gamercard h1 a:hover {
				background: #bbe6a6;
				color: #333;
			}
			
	.gamercard h2 {
		color: #111;
		font-size: 16px;
		font-weight: normal;
		margin-top: 15px;
	}
	
	.gamercard ul {
		list-style-type: none;
	}
		.gamercard ul li {
			padding-top: 8px;
		}
			
			.gamercard ul li strong {
				color: #666;
			}
			
	.gamercard ul.games li {
		display: inline-block; 
		margin-right: 20px;
		text-align: center;
		font-weight: bold;
		width: 85px;
		vertical-align: top;
	}
		.gamercard ul.games li img {
			margin: 0 auto;
			width: 85px;
		}
	
	.gamercard a {
		color: #78bb58;
	}
	
	.gamercard .clear {
		clear: both;
	}
	
</style>

<!-- gamercard -->
<div class="gamercard">
	
	<!-- profile image -->
	<img src="<?php echo $user->avatars->body_gamerpic; ?>" alt="<?php echo $user->gamertag; ?>" class="avatar" />
	
	<!-- gamer name -->
	<h1><img src="<?php echo $user->avatars->gamer_tile; ?>" alt="<?php echo $user->gamertag; ?>" /><a href="<?php echo $user->profile_link; ?>"><?php echo $user->gamertag; ?></a></h1>
	
	<!-- personal info -->
	<h2>The Legend</h2>
	<ul>
		<li><strong>Name:</strong> <?php echo $user->name; ?></li>
		<li><strong>Bio:</strong> <?php echo $user->bio; ?></li>
		<li><strong>Location:</strong> <?php echo $user->location; ?></li>
		<li><strong>Gender:</strong> <?php echo $user->gender; ?></li>
		<li><strong>Motto:</strong> <?php echo $user->motto; ?></li>
		<li><strong>Online:</strong> <?php echo $user->online ? "Online" : "Offline"; ?></li>
		<li><strong>Status:</strong> <?php echo $user->online ? $user->online_status : "(none)"; ?></li>
	</ul>
	
	<!-- recent games -->
	<h2>Recent Games</h2>
	<ul class="games">
		<?php foreach($user->recent_games as $game): ?>
			 <li><a href="<?php echo $game->marketplace_url; ?>"><img src="<?php echo $game->small_boxart; ?>" alt="<?php echo $game->title; ?>" /></a><br /><?php echo $game->title; ?></li> 
		<?php endforeach; ?>
	</ul>
	
	
	<div class="clear"></div>
</div>
