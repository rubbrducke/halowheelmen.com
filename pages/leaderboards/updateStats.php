<?php
/**
 * Created by PhpStorm.
 * User: Josh
 * Date: 3/2/2016
 * Time: 11:08 PM
 */

$speed = isset($_GET["speed"]) ? $_GET["speed"] : 2;

?>
<!DOCTYPE html>
<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="statistics/h5/async.min.js"></script>
    </head>
<body>
<script type="text/javascript">
    $(document).ready(function(){
       $("#gtButton").click(function(){
       		$("#startLabel").text('Fetching games....');
       		$("#doneLabel").text('');
       		$("#infoLabel").text('');
       		$("#failedLabel").text('');
          $("#errorLabel").text('');
           var gamertag = $("#gamertag").val();
           //http://halowheelmen.com/site/pages/leaderboards/statistics/h5/api/statsapi.php/getMatches/HWM%20BrickFungus
            var matches = {};
           var dbMatches = {};
           var playerid;
           var toomanygame = false;
           $.ajax({
               method: "GET",
               url: "statistics/h5/api/statsapi.php/getPlayerID/" + gamertag,
               dataType: "json",
               async: false
           }).done(function(data){
               playerid = data;
           });
           console.log('playerid:',playerid);
            matches = 'more';
           while(matches === 'more') {
               $.ajax({
                   method: "GET",
                   url: "statistics/h5/api/statsapi.php/getMatches/" + gamertag,
                   dataType: "json",
                   async: false
               }).done(function (data) {
                   matches = data;
                   if(matches === 'more'){
                       $("#infoLabel").text('so many games, pulling more');
                   }
                   else if (matches.length === 0) {
                       $("#infoLabel").text('No games to pull... you are all caught up.  Thanks for playing');
                   }
               }).fail(function () {
                   $("#failedLabel").text('you have too many game to pull... thanks for being so awesome.  contact brick for assistance');
                   toomanygame = true;

               });
               console.log('matches', matches);
           }
            console.log('size:',matches.length);
           $.ajax({
               method: "GET",
               url: "statistics/h5/api/statsapi.php/getDbGames/" + gamertag,
               dataType: "json",
               async: false
           }).done(function(data){
               dbMatches = data;
               if(dbMatches.length === 0){
                   $("#infoLabel").text('database pull fail');
               }
           }).fail(function() {
               $("#failedLabel").text('database pull fail  contact brick for assistance');
           });

           console.log('db matches',dbMatches);
           console.log('db size:',dbMatches.length);

            newGames = 0;
           completed = 0;
           failedCount = 0;
           total = matches.length;
		
	        arrFailedGames = [];
		
		
		
		

          function run_match(match,callback) {

               var gameMode = match.Id.GameMode;
               var matchid = match.Id.MatchId;
               var gamedate = match.MatchCompletedDate.ISO8601Date;
               console.log(matchid);
              if($.inArray(matchid,dbMatches) === -1) {
                  $.ajax({
                      method: "GET",
                      url: "statistics/h5/api/statsapi.php/runmatch/" + matchid + "/" + playerid + "/" + gameMode + "/" + gamertag + "/" + gamedate,
                      dataType: "json",
                      async: true
                  }).fail(function () {
                      console.error('something went wrong webcall');
                      failedCount = failedCount + 1;
                      $("#failedLabel").text(failedCount);

                      arrFailedGames.push(match);
                  }).done(function (data) {
                      console.log('data: ' + data);
                      completed = completed + 1;
                      newGames = newGames + 1;
                      var display = completed + "/" + total + " : " + newGames + " new games collected";
                      console.log(display);
                      $("#infoLabel").text(display);

                  }).always(function () {
                      callback();
                  });
              } else {
                  completed = completed + 1;
                  var display = completed + "/" + total + " : " + newGames + " new games collected";
                  console.log(display);
                  $("#infoLabel").text(display);
                  callback();
              }
               	
           }
           

           if(!toomanygame) {
             var queue = async.queue(run_match, <?= $speed?>); // Run 2 simultaneous uploads
  		      failIterator = 0;
  		      queue.drain = function() {
          while(failIterator < 10 && arrFailedGames.length > 0) {
            failedCount = 0;
            $("#errorLabel").text('Processing ' + arrFailedGames.length + ' failed games');
            var newMatches = arrFailedGames;
            arrFailedGames = [];
            this.push(newMatches );
            failIterator = failIterator + 1;
            if(failIterator == 10) {
              $("#doneLabel").text('DONE! and DONE... we\'re not getting those ' + newMatches.length + ' failed games');
            }
          }
          if(failIterator < 10){
  		    $("#doneLabel").text('DONE!');
        }
      }
		};
		
		// Queue your files for upload
		queue.push(matches);
	/*	
           if(total == 0){
               $("#doneLabel").text('DONE!');
           }*/
           
    
           
   });
});

</script>

Gamertag:<input id="gamertag" ><button id="gtButton">Update stats</button>
<br><br>
<span id="startLabel"></span><br><br>
Games to process:<div id="infoBox" class="infoBox"><span id="infoLabel"></span></div>
Failed games:<div id="failedBox" class="failedBox"><span id="failedLabel"></span></div>
<div id="doneBox" class="doneBox"><span id="doneLabel"></span></div>
<div id="errorBox" class="errorBox"><span id="errorLabel"></span></div>
</body>
</html>