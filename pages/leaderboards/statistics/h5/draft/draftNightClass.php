<?php

/**

 * Created by PhpStorm.

 * User: Josh

 * Date: 8/24/2016

 * Time: 10:12 PM

 */

//ini_set('display_errors',1);

class draftNightClass {

    var $mysql;
    var $arrPlayers;
    var $arrPlayers1;
    var $arrPlayers2;
    var $arrGames;
    var $draftID;

    var $team1Name;
    var $team2Name;
    var $team1TotalScore;
    var $team2TotalScore;

    var $mostBaseDef;
    var $mostBaseDefCount = 0;
    var $bestAccuracy;
    var $bestAccuracyCount = 0;
    var $worstAccuracy;
    var $worstAccuracyCount = 1;
    var $highestScore;
    var $highestScoreCount = 0;
    var $mostSplatters;
    var $mostSplattersCount = 0;
    var $mostRoadtrips;
    var $mostRoadtripsCount = 0;
    var $mostProtectorsAndGuardianAngels;
    var $mostPnACount = 0;
    var $mostAssists;
    var $mostAssistsCount = 0;
    var $mostVehicleDestruction;
    var $mostVehicleDestructionCount = 0;
    var $shortestLifeSpan;
    var $shortestLifeSpanCount = 9999;
    var $longestLifeSpan;
    var $longestLifeSpanCount = 0;
    var $mostBaseCap;
    var $mostBaseCapCount = 0;
    var $mostWheelmen;
    var $mostWheelmenCount = 0;

    function __construct($draftID){

        $this->mysql = new mysqli('localhost', 'halowhee_hwmPub', 'youdieW0me', 'halowhee_statistics');


        $this->draftID = $draftID;
        $this->getGames();
        $strGameList = "'".implode("','",$this->arrGames)."'";
        //set team names
        $strSQL = "SELECT * FROM  `draft_night_player` WHERE  `draft_night_id` =$draftID AND  `leader` =1";
        $result = $this->mysql->query($strSQL);

        $row = $result->fetch_assoc();
        $this->team1Name = $row["team"];
        $team1Leader = $row["player_id"];


        $row = $result->fetch_assoc();
        $this->team2Name = $row["team"];
        $team2Leader = $row["player_id"];

        $strSQL = "select sum(gt.score) totalScore from game_team gt
        inner join game_player gp on gt.gameId = gp.game_id and gp.player_id = $team1Leader and gp.teamId = gt.teamId
        where gt.gameId in ($strGameList)";

        $result = $this->mysql->query($strSQL);
        $row = $result->fetch_assoc();
        $this->team1TotalScore = $row["totalScore"];

        $strSQL = "select sum(gt.score) totalScore from game_team gt
        inner join game_player gp on gt.gameId = gp.game_id and gp.player_id = $team2Leader and gp.teamId = gt.teamId
        where gt.gameId in ($strGameList)";
        $result = $this->mysql->query($strSQL);
        $row = $result->fetch_assoc();
        $this->team2TotalScore = $row["totalScore"];




        $this->getPlayers();
        $this->processAwards();
    }



    function getGames()
    {
        $draftID = $this->draftID;
        $strSQL = "select game_id from draft_night_game where draft_night_id = $draftID";
        $result = $this->mysql->query($strSQL);
        for ($i = 0; $i < $result->num_rows; $i++) {
            $row = $result->fetch_assoc();
            $this->arrGames[] = $row["game_id"];
        }
    }

    function getPlayers()
    {
        $draftID = $this->draftID;
        $strSQL = "select player_id, team, gamertag from draft_night_player gnp inner join player p on p.id = gnp.player_id where draft_night_id = $draftID";
        $result = $this->mysql->query($strSQL);
        for ($i = 0; $i < $result->num_rows; $i++) {
            $row = $result->fetch_assoc();
            $player = new player($row["player_id"],$row["gamertag"],$this->arrGames);
            $player->loadStats();
            $this->arrPlayers[] = $player;
            if($row["team"] == $this->team1Name){
                $this->arrPlayers1[] = $player;
            } else {
                $this->arrPlayers2[] = $player;
            }
        }

    }

    function processAwards()
    {
        for($i=0;$i<sizeof($this->arrPlayers);$i++){
            $player = $this->arrPlayers[$i];
            if($player->totalBaseDefense > $this->mostBaseDefCount) {
                $this->mostBaseDefCount = $player->totalBaseDefense;
                $this->mostBaseDef = $player->gamerTag;
            }

            if($player->totalBaseCaptures > $this->mostBaseCapCount) {
                $this->mostBaseCapCount = $player->totalBaseCaptures;
                $this->mostBaseCap = $player->gamerTag;
            }

            if($player->totalShotsFired != 0 && $player->totalShotsFired != '') {
                if (($player->totalShotsLanded / $player->totalShotsFired) > $this->bestAccuracyCount) {
                    $this->bestAccuracyCount = ($player->totalShotsLanded / $player->totalShotsFired);
                    $this->bestAccuracy = $player->gamerTag;
                }
            }

            if($player->totalShotsFired != 0 && $player->totalShotsFired != '') {
                if (($player->totalShotsLanded / $player->totalShotsFired) < $this->worstAccuracyCount) {
                    $this->worstAccuracyCount = ($player->totalShotsLanded / $player->totalShotsFired);
                    $this->worstAccuracy = $player->gamerTag;
                }
            }


            if($player->totalSplatters > $this->mostSplattersCount) {
                $this->mostSplattersCount = $player->totalSplatters;
                $this->mostSplatters = $player->gamerTag;
            }



            if($player->totalRoadtrips > $this->mostRoadtripsCount) {
                $this->mostRoadtripsCount = $player->totalRoadtrips;
                $this->mostRoadtrips = $player->gamerTag;
            }



            if(($player->totalProtectors + $player->totalGuardianAngels) > $this->mostPnACount) {
                $this->mostPnACount = ($player->totalProtectors + $player->totalGuardianAngels);
                $this->mostProtectorsAndGuardianAngels = $player->gamerTag;
            }


            if($player->assists > $this->mostAssistsCount) {
                $this->mostAssistsCount = $player->assists;
                $this->mostAssists = $player->gamerTag;
            }

            if($player->totalWheelmen > $this->mostWheelmenCount) {
                $this->mostWheelmenCount = $player->totalWheelmen;
                $this->mostWheelmen = $player->gamerTag;
            }

            $vehicleDestruction = $player->warthogDestructions + $player->totalBansheeDestruction + $player->totalGhostDestruction + $player->totalMantisDestruction + $player->totalMongooseDestruction + $player->totalPhaetonDestruction + $player->totalScorpionDestruction + $player->totalWaspDestruction + $player->totalWraithDestruction + $player->totalBansheeAssist + $player->totalGhostAssist + $player->totalMantisAssist + $player->totalMongooseAssist + $player->totalPhaetonAssist + $player->totalScorpionAssist + $player->totalWaspAssist + $player->totalWraithAssist + $player->warthogAssists;

            if($vehicleDestruction > $this->mostVehicleDestructionCount) {
                $this->mostVehicleDestructionCount = $vehicleDestruction;
                $this->mostVehicleDestruction = $player->gamerTag;
            }

            if($player->aveLifespan > $this->longestLifeSpanCount) {
                $this->longestLifeSpanCount = $player->aveLifespan;
                $this->longestLifeSpan = $player->gamerTag;
            }
            if($player->aveLifespan != '') {
                if ($player->aveLifespan < $this->shortestLifeSpanCount) {
                    $this->shortestLifeSpanCount = $player->aveLifespan;
                    $this->shortestLifeSpan = $player->gamerTag;
                }
            }
        }
        $this->bestAccuracyCount *= 100;
        $this->worstAccuracyCount *= 100;


    }

    function getPlayer($gamertag)
    {
      for($i=0;$i<sizeof($this->arrPlayers);$i++){
        $player = $this->arrPlayers[$i];
        if($player->gamerTag == $gamertag) {
          return $player;
        }
      }
    }
}



class player {

    var $gameCount;
    var $gamerTag;
    var $playerID;
    var $gameList;
    var $teamID;
    var $captain;

    //game_player
    var $kills;
    var $assists;
    var $deaths;
    var $aveRank;
    var $aveLifespan;
    var $totalHeadshots;
    var $totalWeaponDamage;
    var $totalShotsFired;
    var $totalShotsLanded;
    var $totalAssassinations;
    var $totalGrenadeKills;
    var $totalGroundPoundKills;
    var $totalShoulderBashKills;
    var $totalPowerWeaponKills;
    var $totalPowerWeaponGrabs;
    var $totalSpartanKills;

    //medals
    var $totalWheelmen;
    var $totalBaseDefense;
    var $totalEliteKills;
    var $totalSoldierKills;
    var $totalKnightKills;
    var $totalCrawlerKills;
    var $totalWatcherKills;
    var $totalJackalKills;
    var $totalGruntKills;
    var $totalMarineKills;
    var $totalCoreDefense;
    var $totalBaseCaptures;
    var $totalSplatters;
    var $totalRoadtrips;
    var $totalProtectors;
    var $totalGuardianAngels;
    var $warthogDestructions;
    var $warthogAssists;

    var $totalWaspDestruction;
    var $totalWaspAssist;
    var $totalWraithDestruction;
    var $totalWraithAssist;
    var $totalMantisDestruction;
    var $totalMantisAssist;
    var $totalGhostDestruction;
    var $totalGhostAssist;
    var $totalBansheeDestruction;
    var $totalBansheeAssist;
    var $totalMongooseDestruction;
    var $totalMongooseAssist;
    var $totalPhaetonDestruction;
    var $totalPhaetonAssist;
    var $totalScorpionDestruction;
    var $totalScorpionAssist;


    //weapons
    var $totalScorpionKills;
    var $totalPhaetonKills;
    var $totalRailgunKills;
    var $totalMantisKills;
    var $totalBansheeKills;
    var $totalWraithKills;
    var $totalWarthogKills;
    var $totalGhostKills;
    var $totalMongooseKills;
    var $totalNeedlerKills;
    var $totalLaserKills;
    var $totalShotgunKills;
    var $totalWaspKills;
    var $totalTurretKills;
    var $totalGaussKills;



    function __construct($playerID,$gamertag,$gameList) {
        $this->playerID = $playerID;
        $this->gameList = $gameList;
        $this->gamerTag = $gamertag;

    }

    function loadStats() {
//365
        //18c4234a-b1fa-46d6-9970-418f4d9793a3,44b56422-27fb-4edd-88ee-33adf082b068,e760094b-09b1-429c-9652-e620fbbc927a

        $mysql = new mysqli('localhost', 'halowhee_hwmPub', 'youdieW0me', 'halowhee_statistics');

        $strGameList = "'".implode("','",$this->gameList)."'";
        $playerID = $this->playerID;

        $strSQl = "Select 
           sum(gp.kills) totalKills,
           avg(gp.rank) aveRank,
           sum(gp.assists) totalAssists,
           sum(gp.deaths) totalDeaths,
           avg(gp.AvgLifespan) aveLifespan,
           sum(gp.Headshots) totalHeadshots,
           sum(gp.TotalWeaponDamage) totalWeaponDamage,
           sum(gp.TotalShotsFired) totalShotsFired,
           sum(gp.TotalShotsLanded) totalShotsLanded,
           sum(gp.TotalAssassinations) totalAssassinations,
           sum(gp.TotalGrenadeKills) totalGrenadeKills,
           sum(gp.TotalGroundPoundKills) totalGroundPoundKills,
           sum(gp.TotalShoulderBashKills) totalShoulderBashKills,
           sum(gp.TotalPowerWeaponKills) totalPowerWeaponKills,
           sum(gp.TotalPowerWeaponGrabs) totalPowerWeaponGrabs,
           sum(gp.TotalSpartanKills) totalSpartanKills,

           IFNULL(SUM(wheelmen.count),0) totalWheelmen,
           IFNULL(SUM(basedefense.count),0) totalBaseDefense,
           IFNULL(SUM(elitekill.count),0) totalEliteKills,
           IFNULL(SUM(soldierkill.count),0) totalSoldierKills,
           IFNULL(SUM(knightkill.count),0) totalKnightKills,
           IFNULL(SUM(crawlerkill.count),0) totalCrawlerKills,
           IFNULL(SUM(watcherkill.count),0) totalWatcherKills,
           IFNULL(SUM(jackalkill.count),0) totalJackalKills,
           IFNULL(SUM(gruntkill.count),0) totalGruntKills,
           IFNULL(SUM(marinekill.count),0) totalMarineKills,
           IFNULL(SUM(coredefense.count),0) totalCoreDefense,
           IFNULL(SUM(basecapture.count),0) totalBaseCaptures,
           IFNULL(SUM(warthogdestroy.count),0) totalWarthogDestroy,
           IFNULL(SUM(warthogassist.count),0) totalWarthogAssist,
          IFNULL(SUM(splatters.count),0) totalSplatters,
          IFNULL(SUM(roadtrip.count),0) totalRoadtrips,
          IFNULL(SUM(protector.count),0) totalProtectors,
          IFNULL(SUM(guardianangel.count),0) totalGuardianAngels,

          IFNULL(SUM(waspdestroy.count),0) totalWaspDestroy,
          IFNULL(SUM(waspassist.count),0) totalWaspAssist,
          IFNULL(SUM(wraithdestroy.count),0) totalWraithDestroy,
          IFNULL(SUM(wraithassist.count),0) totalWraithAssist,
          IFNULL(SUM(ghostdestroy.count),0) totalGhostDestroy,
          IFNULL(SUM(ghostassist.count),0) totalGhostAssist,
          IFNULL(SUM(mongoosedestroy.count),0) totalMongooseDestroy,
          IFNULL(SUM(mongooseassist.count),0) totalMongooseAssist,
          IFNULL(SUM(bansheedestroy.count),0) totalBansheeDestroy,
          IFNULL(SUM(bansheeassist.count),0) totalBansheeAssist,
          IFNULL(SUM(mantisdestroy.count),0) totalMantisDestroy,
          IFNULL(SUM(mantisassist.count),0) totalMantisAssist,
          IFNULL(SUM(phaetondestroy.count),0) totalPhaetonDestroy,
          IFNULL(SUM(phaetonassist.count),0) totalPhaetonAssist,
          IFNULL(SUM(scorpiondestroy.count),0) totalScorpionDestroy,
          IFNULL(SUM(scorpionassist.count),0) totalScorpionAssist,



           IFNULL(SUM(scorpionkill.TotalKills), 0) totalScorpionKills,
           IFNULL(SUM(phaetonkill.TotalKills), 0) totalPhaetonKills,
           IFNULL(SUM(railgunkill.TotalKills), 0) totalRailgunKills,
           IFNULL(SUM(mantiskill.TotalKills), 0) totalMantisKills,
           IFNULL(SUM(bansheekill.TotalKills), 0) totalBansheeKills,
           IFNULL(SUM(wraithkill.TotalKills), 0) totalWraithKills,
           IFNULL(SUM(warthogkill.TotalKills), 0) totalWarthogKills,
           IFNULL(SUM(ghostkill.TotalKills), 0) totalGhostKills,
           IFNULL(SUM(mongoosekill.TotalKills), 0) totalMongooseKills,
           IFNULL(SUM(needlerkill.TotalKills), 0) totalNeedleKills,
           IFNULL(SUM(laserkill.TotalKills), 0) totalLaserKills,
           IFNULL(SUM(shotgunkill.TotalKills), 0) totalShotgunKills,
           IFNULL(SUM(waspkill.TotalKills), 0) totalWaspKills,
           IFNULL(SUM(turretkill.TotalKills), 0) totalTurretKills,
           IFNULL(SUM(gausskill.TotalKills), 0) totalGaussKills

           
           from game_player gp
           left join game_player_medal wheelmen on gp.game_id = wheelmen.game_id and gp.player_id = wheelmen.player_id and wheelmen.medal_id = 125834251
           left join game_player_medal basedefense on gp.game_id = basedefense.game_id and gp.player_id = basedefense.player_id and basedefense.medal_id = 2435743433
           left join game_player_medal elitekill on gp.game_id = elitekill.game_id and gp.player_id = elitekill.player_id and elitekill.medal_id = 2955425834
           left join game_player_medal soldierkill on gp.game_id = soldierkill.game_id and gp.player_id = soldierkill.player_id and soldierkill.medal_id = 3344008582
           left join game_player_medal knightkill on gp.game_id = knightkill.game_id and gp.player_id = knightkill.player_id and knightkill.medal_id = 1311847356
           left join game_player_medal crawlerkill on gp.game_id = crawlerkill.game_id and gp.player_id = crawlerkill.player_id and crawlerkill.medal_id = 2947060439
           left join game_player_medal watcherkill on gp.game_id = watcherkill.game_id and gp.player_id = watcherkill.player_id and watcherkill.medal_id = 519459233
           left join game_player_medal jackalkill on gp.game_id = jackalkill.game_id and gp.player_id = jackalkill.player_id and jackalkill.medal_id = 1862589993
           left join game_player_medal gruntkill on gp.game_id = gruntkill.game_id and gp.player_id = gruntkill.player_id and gruntkill.medal_id = 3324603383
           left join game_player_medal marinekill on gp.game_id = marinekill.game_id and gp.player_id = marinekill.player_id and marinekill.medal_id = 1584076385
           left join game_player_medal coredefense on gp.game_id = coredefense.game_id and gp.player_id = coredefense.player_id and coredefense.medal_id = 3505254118
           left join game_player_medal basecapture on gp.game_id = basecapture.game_id and gp.player_id = basecapture.player_id and basecapture.medal_id = 2787661404
           left join game_player_medal warthogdestroy on gp.game_id = warthogdestroy.game_id and gp.player_id = warthogdestroy.player_id and warthogdestroy.medal_id = 92444561
           left join game_player_medal warthogassist on gp.game_id = warthogassist.game_id and gp.player_id = warthogassist.player_id and warthogassist.medal_id = 4048801324
            left join game_player_medal splatters on gp.game_id = splatters.game_id and gp.player_id = splatters.player_id and splatters.medal_id = 3676723563
           left join game_player_medal roadtrip on gp.game_id = roadtrip.game_id and gp.player_id = roadtrip.player_id and roadtrip.medal_id = 2859802775
            left join game_player_medal protector on gp.game_id = protector.game_id and gp.player_id = protector.player_id and protector.medal_id = 2838259753
            left join game_player_medal guardianangel on gp.game_id = guardianangel.game_id and gp.player_id = guardianangel.player_id and guardianangel.medal_id = 352859864

            left join game_player_medal waspdestroy on gp.game_id = waspdestroy.game_id and gp.player_id = waspdestroy.player_id and waspdestroy.medal_id = 1711392399
            left join game_player_medal waspassist on gp.game_id = waspassist.game_id and gp.player_id = waspassist.player_id and waspassist.medal_id = 3865042769
            left join game_player_medal wraithdestroy on gp.game_id = wraithdestroy.game_id and gp.player_id = wraithdestroy.player_id and wraithdestroy.medal_id = 1568252876
            left join game_player_medal wraithassist on gp.game_id = wraithassist.game_id and gp.player_id = wraithassist.player_id and wraithassist.medal_id = 2237392606
            left join game_player_medal ghostdestroy on gp.game_id = ghostdestroy.game_id and gp.player_id = ghostdestroy.player_id and ghostdestroy.medal_id = 2497544753
            left join game_player_medal ghostassist on gp.game_id = ghostassist.game_id and gp.player_id = ghostassist.player_id and ghostassist.medal_id = 3824002610
            left join game_player_medal mongoosedestroy on gp.game_id = mongoosedestroy.game_id and gp.player_id = mongoosedestroy.player_id and mongoosedestroy.medal_id = 515648662
            left join game_player_medal mongooseassist on gp.game_id = mongooseassist.game_id and gp.player_id = mongooseassist.player_id and mongooseassist.medal_id = 2732907792
            left join game_player_medal bansheedestroy on gp.game_id = bansheedestroy.game_id and gp.player_id = bansheedestroy.player_id and bansheedestroy.medal_id = 762229696
            left join game_player_medal bansheeassist on gp.game_id = bansheeassist.game_id and gp.player_id = bansheeassist.player_id and bansheeassist.medal_id = 786413504
            left join game_player_medal mantisdestroy on gp.game_id = mantisdestroy.game_id and gp.player_id = mantisdestroy.player_id and mantisdestroy.medal_id = 615190505
            left join game_player_medal mantisassist on gp.game_id = mantisassist.game_id and gp.player_id = mantisassist.player_id and mantisassist.medal_id = 3440416044
            left join game_player_medal phaetondestroy on gp.game_id = phaetondestroy.game_id and gp.player_id = phaetondestroy.player_id and phaetondestroy.medal_id = 1795642208
            left join game_player_medal phaetonassist on gp.game_id = phaetonassist.game_id and gp.player_id = phaetonassist.player_id and phaetonassist.medal_id = 3718365815
            left join game_player_medal scorpiondestroy on gp.game_id = scorpiondestroy.game_id and gp.player_id = scorpiondestroy.player_id and scorpiondestroy.medal_id = 35545941
            left join game_player_medal scorpionassist on gp.game_id = scorpionassist.game_id and gp.player_id = scorpionassist.player_id and scorpionassist.medal_id = 3392925967


           left join game_player_weapon scorpionkill on gp.game_id = scorpionkill.game_id and gp.player_id = scorpionkill.player_id and scorpionkill.weapon_id = 1730553442
           left join game_player_weapon phaetonkill on gp.game_id = phaetonkill.game_id and gp.player_id = phaetonkill.player_id and phaetonkill.weapon_id = 3394982816
           left join game_player_weapon railgunkill on gp.game_id = railgunkill.game_id and gp.player_id = railgunkill.player_id and railgunkill.weapon_id = 3682788176
           left join game_player_weapon mantiskill on gp.game_id = mantiskill.game_id and gp.player_id = mantiskill.player_id and mantiskill.weapon_id = 3227919741
           left join game_player_weapon bansheekill on gp.game_id = bansheekill.game_id and gp.player_id = bansheekill.player_id and bansheekill.weapon_id = 419783896
           left join game_player_weapon wraithkill on gp.game_id = wraithkill.game_id and gp.player_id = wraithkill.player_id and wraithkill.weapon_id = 1206711506
           left join game_player_weapon warthogkill on gp.game_id = warthogkill.game_id and gp.player_id = warthogkill.player_id and warthogkill.weapon_id = 4028516791
           left join game_player_weapon ghostkill on gp.game_id = ghostkill.game_id and gp.player_id = ghostkill.player_id and ghostkill.weapon_id = 3010146366
           left join game_player_weapon mongoosekill on gp.game_id = mongoosekill.game_id and gp.player_id = mongoosekill.player_id and mongoosekill.weapon_id = 1063919886
           left join game_player_weapon needlerkill on gp.game_id = needlerkill.game_id and gp.player_id = needlerkill.player_id and needlerkill.weapon_id = 2050745863
           left join game_player_weapon laserkill on gp.game_id = laserkill.game_id and gp.player_id = laserkill.player_id and laserkill.weapon_id = 3885603197
           left join game_player_weapon shotgunkill on gp.game_id = shotgunkill.game_id and gp.player_id = shotgunkill.player_id and shotgunkill.weapon_id = 3484334713
           left join game_player_weapon waspkill on gp.game_id = waspkill.game_id and gp.player_id = waspkill.player_id and waspkill.weapon_id = 3207900961
           left join game_player_weapon turretkill on gp.game_id = turretkill.game_id and gp.player_id = turretkill.player_id and turretkill.weapon_id = 2988661926
           left join game_player_weapon gausskill on gp.game_id = gausskill.game_id and gp.player_id = gausskill.player_id and gausskill.weapon_id = 4233134183
           
           where gp.player_id = $playerID
           and gp.game_id in ($strGameList)


        ";
        //echo $strSQl;
        $statresult = $mysql->query($strSQl);

        if($statresult) {
            $row = $statresult->fetch_assoc();

            $this->kills = $row["totalKills"];
            $this->aveRank = $row["aveRank"];
            $this->assists = $row["totalAssists"];
            $this->deaths = $row["totalDeaths"];
            $this->aveLifespan = $row["aveLifespan"];
            $this->totalHeadshots = $row["totalHeadshots"];
            $this->totalWeaponDamage = $row["totalWeaponDamage"];
            $this->totalShotsFired = $row["totalShotsFired"];
            $this->totalShotsLanded = $row["totalShotsLanded"];
            $this->totalAssassinations = $row["totalAssassinations"];
            $this->totalGrenadeKills = $row["totalGrenadeKills"];
            $this->totalGroundPoundKills = $row["totalGroundPoundKills"];
            $this->totalShoulderBashKills = $row["totalShoulderBashKills"];
            $this->totalPowerWeaponKills = $row["totalPowerWeaponKills"];
            $this->totalPowerWeaponGrabs = $row["totalPowerWeaponGrabs"];
            $this->totalSpartanKills = $row["totalSpartanKills"];


            $this->totalWheelmen = $row["totalWheelmen"];
            $this->totalBaseDefense = $row["totalBaseDefense"];
            $this->totalEliteKills = $row["totalEliteKills"];
            $this->totalSoldierKills = $row["totalSoldierKills"];
            $this->totalKnightKills = $row["totalKnightKills"];
            $this->totalCrawlerKills = $row["totalCrawlerKills"];
            $this->totalWatcherKills = $row["totalWatcherKills"];
            $this->totalJackalKills = $row["totalJackalKills"];
            $this->totalGruntKills = $row["totalGruntKills"];
            $this->totalMarineKills = $row["totalMarineKills"];
            $this->totalCoreDefense = $row["totalCoreDefense"];
            $this->totalBaseCaptures = $row["totalBaseCaptures"];
            $this->warthogDestructions = $row["totalWarthogDestroy"];
            $this->warthogAssists = $row["totalWarthogAssist"];
            $this->totalSplatters = $row["totalSplatters"];
            $this->totalRoadtrips = $row["totalRoadtrips"];
            $this->totalProtectors = $row["totalProtectors"];
            $this->totalGuardianAngels = $row["totalGuardianAngels"];

            $this->totalWaspDestruction = $row["totalWaspDestroy"];
            $this->totalWaspAssist = $row["totalWaspAssist"];
            $this->totalWraithDestruction = $row["totalWraithDestroy"];
            $this->totalWraithAssist = $row["totalWraithAssist"];
            $this->totalGhostDestruction = $row["totalGhostDestroy"];
            $this->totalGhostAssist = $row["totalGhostAssist"];
            $this->totalMongooseDestruction = $row["totalMongooseDestroy"];
            $this->totalMongooseAssist = $row["totalMongooseAssist"];
            $this->totalBansheeDestruction = $row["totalBansheeDestroy"];
            $this->totalBansheeAssist = $row["totalBansheeAssist"];
            $this->totalMantisDestruction = $row["totalMantisDestroy"];
            $this->totalMantisAssist = $row["totalMantisAssist"];
            $this->totalPhaetonDestruction = $row["totalPhaetonDestroy"];
            $this->totalPhaetonAssist = $row["totalPhaetonAssist"];
            $this->totalScorpionDestruction = $row["totalScorpionDestroy"];
            $this->totalScorpionAssist = $row["totalScorpionAssist"];







            $this->totalScorpionKills = $row["totalScorpionKills"];
            $this->totalPhaetonKills = $row["totalPhaetonKills"];
            $this->totalRailgunKills = $row["totalRailgunKills"];
            $this->totalMantisKills = $row["totalMantisKills"];
            $this->totalBansheeKills = $row["totalBansheeKills"];
            $this->totalWraithKills = $row["totalWraithKills"];
            $this->totalWarthogKills = $row["totalWarthogKills"];
            $this->totalGhostKills = $row["totalGhostKills"];
            $this->totalMongooseKills = $row["totalMongooseKills"];
            $this->totalNeedlerKills = $row["totalNeedleKills"];
            $this->totalLaserKills = $row["totalLaserKills"];
            $this->totalShotgunKills = $row["totalShotgunKills"];
            $this->totalWaspKills = $row["totalWaspKills"];
            $this->totalTurretKills = $row["totalTurretKills"];
            $this->totalGaussKills = $row["totalGaussKills"];
        } else {
            echo "MYSQL ERROR:".$mysql->error;
        }
    }

}