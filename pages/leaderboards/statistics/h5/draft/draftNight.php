<?php
require("draftNightClass.php");
//ini_set('display_errors',1);
$mysqli = new mysqli('localhost', 'halowhee_hwmPub', 'youdieW0me', 'halowhee_statistics');
$mysqli->set_charset("utf8");

if(!empty($_POST)) {

    $draftID = $_POST["draft_night_id"];
    $draftID = $mysqli->real_escape_string($draftID);
    $strSQL = "select * from draft_night_object where draft_night_id = $draftID";

   $result = $mysqli->query($strSQL);
   $row = $result->fetch_assoc();
   $draftNight = unserialize($row["draft_night_object"]);
  
} else {

    $draftID = -1;

}


$strSQL = "SELECT dn.id draftid, p1.gamertag p1GT, p2.gamertag p2GT, dnp1.team p1Team, dnp2.team p2Team

FROM `draft_night` dn

inner join draft_night_player dnp1 on dn.id = dnp1.draft_night_id and dnp1.leader = 1

inner join player p1 on dnp1.player_id = p1.id

inner join draft_night_player dnp2 on dn.id = dnp2.draft_night_id and dnp2.leader = 1 and dnp2.player_id <> dnp1.player_id

inner join player p2 on dnp2.player_id = p2.id

group by dn.id

order by dn.id desc";



$result = $mysqli->query($strSQL);
?>
<html>

<head>


<link rel="stylesheet" href="style.css" />
</head>

<body>

<form action="" method="post">

DRAFT NIGHT: <select name="draft_night_id">

    <?php

    while ($row = $result->fetch_assoc()) {

        if($draftID == -1){

            $draftID = $row["draftid"];

        }

        echo '<option value="'.$row["draftid"].'">'.$row["p1Team"].' vs '.$row["p2Team"].'</option>';

    }

    ?></select>



    <input type="submit" value"View this draft night">

    </form>


<?php
 if($draftNight) {
    $strSQL = "SELECT p.gamertag 
                FROM  `draft_night_player` dnp
                INNER JOIN player p ON dnp.player_id = p.id
                WHERE dnp.team =  '".$draftNight->team1Name."'
                AND leader =1";
    $result = $mysqli->query($strSQL);
    $row = $result->fetch_assoc();
    $team1Leader = $row["gamertag"];

    $strSQL = "SELECT p.gamertag 
                FROM  `draft_night_player` dnp
                INNER JOIN player p ON dnp.player_id = p.id
                WHERE dnp.team =  '".$draftNight->team2Name."'
                AND leader =1";
    $result = $mysqli->query($strSQL);
    $row = $result->fetch_assoc();
    $team2Leader = $row["gamertag"];
?>

<div id="teambox">
    <div id="teamboxleft" class="teamboxleft">
        <div class="teamname"><?=$draftNight->team1Name?><br>Team leader: <?= $team1Leader?><br>Total score: <?= $draftNight->team1TotalScore?></div>
        <table>
            <tr>
                <td>Player</td>
                <td>Kills</td>
                <td>Deaths</td>
                <td>Assists</td>
                <td>K/D</td>
                <td>Accuracy</td>
                <td>Ave Lifespan</td>
                <td>Headshots</td>
                <td>Wheelmen</td>
                <td>Base def</td>
                <td>Base Cap</td>
            </tr>
            <?php
                for($i=0;$i<sizeof($draftNight->arrPlayers1);$i++) {
                    $player = $draftNight->arrPlayers1[$i];
                    ?>
                <tr>
                    <td><?= $player->gamerTag?></td>
                    <td><?= $player->kills?></td>
                    <td><?= $player->deaths?></td>
                    <td><?= $player->assists?></td>
                    <td><? if($player->deaths != 0  && $player->deaths != '') echo round($player->kills/$player->deaths,1); ?></td>
                    <td><? if($player->totalShotsFired != 0  && $player->totalShotsFired != '') echo round($player->totalShotsLanded/$player->totalShotsFired,1); ?></td>
                    <td><?= round($player->aveLifespan,1).' sec'?></td>
                    <td><?= $player->totalHeadshots?></td>
                    <td><?= $player->totalWheelmen ?></td>
                    <td><?= $player->totalBaseDefense ?></td>
                    <td><?= $player->totalBaseCaptures ?></td>
                </tr>
                <? }?>
                </table>
            
    </div>
    <div id="teamboxright" class="teamboxright">
        <div class="teamname"><?=$draftNight->team2Name?><br>Team leader: <?= $team2Leader?><br>Total score: <?= $draftNight->team2TotalScore?></div>
        <table>
            <tr>
                <td>Player</td>
                <td>Kills</td>
                <td>Deaths</td>
                <td>Assists</td>
                <td>K/D</td>
                <td>Accuracy</td>
                <td>Ave Lifespan</td>
                <td>Headshots</td>
                <td>Wheelmen</td>
                <td>Base def</td>
                <td>Base Cap</td>
            </tr>
            <?php
                for($i=0;$i<sizeof($draftNight->arrPlayers2);$i++) {
                    $player = $draftNight->arrPlayers2[$i];
                    ?>
                <tr>
                    <td><?= $player->gamerTag?></td>
                    <td><?= $player->kills?></td>
                    <td><?= $player->deaths?></td>
                    <td><?= $player->assists?></td>
                    <td><? if($player->deaths != 0  && $player->deaths != '') echo round($player->kills/$player->deaths,1); ?></td>
                    <td><? if($player->totalShotsFired != 0  && $player->totalShotsFired != '') echo round($player->totalShotsLanded/$player->totalShotsFired,1); ?></td>
                    <td><?= round($player->aveLifespan,1).' sec'?></td>
                    <td><?= $player->totalHeadshots?></td>
                    <td><?= $player->totalWheelmen ?></td>
                    <td><?= $player->totalBaseDefense ?></td>
                    <td><?= $player->totalBaseCaptures ?></td>
                </tr>
                <? }?>
                </table>
    </div>

    <div class="awards">
        <br><h2>AWARDS</h2><br>
        <table class="awardsTable">
        <tr><td>Dikembe Mutombo (most base defenses)</td><td> <?= $draftNight->mostBaseDef?></td><td><?=$draftNight->mostBaseDefCount?></td></tr>
        <tr><td>Spray N Pray (worst accuracy)</td><td> <?= $draftNight->worstAccuracy?></td><td><?=round($draftNight->worstAccuracyCount,1)?></td></tr>
        <tr><td>Sharpshooter (best accuracy)</td><td> <?= $draftNight->bestAccuracy?></td><td><?=round($draftNight->bestAccuracyCount,1)?></td></tr>
        <tr><td>That's never happend to me before... (shortest lifespan)</td><td> <?= $draftNight->shortestLifeSpan?></td><td><?=round($draftNight->shortestLifeSpanCount,1).' sec'?></td></tr>
        <tr><td>Camper (longest lifespan)</td><td> <?= $draftNight->longestLifeSpan?></td><td><?=round($draftNight->longestLifeSpanCount,1).' sec'?></td></tr>
        <tr><td>Don't be a hero (most base caps)</td><td> <?= $draftNight->mostBaseCap?></td><td><?=$draftNight->mostBaseCapCount?></td></tr>
        <tr><td>Sarge will be speaking with you soon (most roadtrips)</td><td> <?= $draftNight->mostRoadtrips?></td><td><?=$draftNight->mostRoadtripsCount?></td></tr>
        <tr><td>Why we made this site (most wheelmen)</td><td> <?= $draftNight->mostWheelmen?></td><td><?=$draftNight->mostWheelmenCount?></td></tr>
        <tr><td>Who is wheels? (most splatters)</td><td> <?= $draftNight->mostSplatters?></td><td><?=$draftNight->mostSplattersCount?></td></tr>
        <tr><td>Lifeguard (most protectors &amp; guardian angels)</td><td> <?= $draftNight->mostProtectorsAndGuardianAngels?></td><td><?=$draftNight->mostPnACount?></td></tr>
        <tr><td>Wingman (most assists)</td><td> <?= $draftNight->mostAssists?></td><td><?=$draftNight->mostAssistsCount?></td></tr>
        <tr><td>Laser humper (most vehicle destruction)</td><td> <?= $draftNight->mostVehicleDestruction?></td><td><?=$draftNight->mostVehicleDestructionCount?></td></tr>

        </table>

    </div>
</div>


<?
}
?>

</body>



</html>

