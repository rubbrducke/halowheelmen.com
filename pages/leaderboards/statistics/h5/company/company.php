<?php

require('../statGlobals.php');

$result = $mysqli->query("Select * from company_board where type = 'kill'");
$data = '';
$i=0;
while($row = $result->fetch_assoc()){
	$currentCount = $row["count"];
    $amountNeeded = $row["total"];
    $aveCount = $row["projected"];
    $amountLeft = $amountNeeded - $currentCount;




    if($aveCount == 0) {
        $velocity = 999;
    } else {
        $velocity = $amountLeft / $aveCount;
    }

	if($amountLeft === 0){
		$strSQL = "select * from company where name = '".$row["name"]."' and percentage = 1 order by updateDate asc limit 0,1";
		$result2 = $mysqli->query($strSQL);
		$dateRow = $result2->fetch_assoc();

		$estDate = date("M jS Y",strtotime($dateRow["updateDate"]));
    }else {
        $velocity = intval($velocity);
    	$estDate = date("M jS Y",strtotime("+ $velocity days"));
    }
	if($row['level'] == 'Level 5/5' && $row['percentage'] == 1) {
        $projectedAmount = 0;
    } else {
        $projectedAmount = round($row['projected'],0);
    }

	$data .= '
        data['.$i.'] = {
            name: "'.preg_replace_callback("/(&#[0-9]+;)/", function($m) { return mb_convert_encoding($m[1], "UTF-8", "HTML-ENTITIES"); }, $row['name']) .'",
            description: "'.$row['description'].'",
            level: "'.$row['level'].'",
            progress: "'.$row['count'].'/'.$row['total'].'",
            percentage: "'.round($row['percentage']*100,2).'",
            averageCount: "'.$projectedAmount.'",
            estDate: "'.$estDate.'"
        };
    ';

    $i++;
    $updateDate = $row["updateDate"];
}

?>
<html>
<head>
<link rel="stylesheet" href="../SlickGrid-2.2.6/slick.grid.css" type="text/css"/>
  <link rel="stylesheet" href="../SlickGrid-2.2.6/css/smoothness/jquery-ui-1.11.3.custom.css" type="text/css"/>
<link rel="stylesheet" href="company.css" type="text/css"/>

  <script src="../SlickGrid-2.2.6/lib/jquery-1.11.2.min.js"></script>
<script src="../SlickGrid-2.2.6/lib/jquery.event.drag-2.2.js"></script>

<script src="../SlickGrid-2.2.6/slick.core.js"></script>
<script src="../SlickGrid-2.2.6/slick.grid.js"></script>
  </head>

  <body>
  <div id="myGrid" style="width:100%;height:100%;"></div>
  <script type="text/javascript">
    var grid;
function NumericSorter(a, b) {
  var x = parseFloat(a[sortcol]), y = parseFloat(b[sortcol]);
  return sortdir * (x == y ? 0 : (x > y ? 1 : -1));
};
function sorterStringCompare(a, b) {
    var x = a[sortcol], y = b[sortcol];
    return sortdir * (x === y ? 0 : (x > y ? 1 : -1));
};
function dateSorter(a, b) {	
	var arrX = a[sortcol].split(' ');
	var arrY = b[sortcol].split(' ');	
	var x = new Date(arrX[2],("JanFebMarAprMayJunJulAugSepOctNovDec".indexOf(arrX[0]) / 3  ),arrX[1].replace(/th/g,'').replace(/st/g,'').replace(/nd/g,'').replace(/rd/g,''));
	var y = new Date(arrY[2],("JanFebMarAprMayJunJulAugSepOctNovDec".indexOf(arrY[0]) / 3  ),arrY[1].replace(/th/g,'').replace(/st/g,'').replace(/nd/g,'').replace(/rd/g,''));
	
	x = x.getTime();
	y = y.getTime();
	return sortdir * (x === y ? 0 : (x > y ? 1 : -1));
}
    var columns = [
        {id:"name", name:"Name", field:"name", sortable: true, width: 275,sorter:sorterStringCompare},
        {id:"description", name:"Description", field:"description", sortable: true, width:500,sorter:sorterStringCompare},
        {id:"level", name:"Level", field:"level", sortable: true, width: 100,sorter:sorterStringCompare},
        {id:"progress", name:"Progress", field:"progress", sortable: true, width:150,sorter:sorterStringCompare},
        {id:"percentage", name:"%", field:"percentage", sortable: true, width:60,sorter:NumericSorter},
        {id:"avecount",name:"Ave",field:"averageCount", sortable: true, width: 60, sorter:NumericSorter},
        {id:"estdate",name:"Est Completion",field: "estDate", sortable: true, width: 150, sorter:dateSorter}
    ];

    var options = {
        enableCellNavigation: false,
        enableColumnReorder: false,
        fullWidthRows: true,
        multiColumnSort: true
    };

    $(function() {
        var data = [];
        <?php echo $data; ?> //This is where we echo the PHP variable $data which contains our JavaScript array as a string.
        console.log(data);
        grid = new Slick.Grid($("#myGrid"), data, columns, options);
        
      	grid.onSort.subscribe(function(e, args) {
	    	sortdir = args.sortCols[0].sortAsc ? 1 : -1;
	    	sortcol = args.sortCols[0].sortCol.field;

	    	data.sort(args.sortCols[0].sortCol.sorter, sortdir);
	    	args.grid.invalidateAllRows();
	    	args.grid.render();
	 	});
    })
</script>	
<br><br>
Last updated: <?php echo date("M jS Y H:i",strtotime($updateDate)) ?>
  </body>
  </html>