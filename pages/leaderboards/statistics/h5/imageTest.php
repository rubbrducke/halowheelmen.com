<?php
// This sample uses the Apache HTTP client from HTTP Components (http://hc.apache.org/httpcomponents-client-ga/)
ini_set("include_path", '/home/halowhee/php:' . ini_get("include_path")  );
require_once 'HTTP/Request2.php';

$request = new Http_Request2('https://www.haloapi.com/profile/h5/profiles/HWM Brickfungus/emblem');

$url = $request->getUrl();

$headers = array(
    // Request headers
    'Ocp-Apim-Subscription-Key' => '376b2fc9890441988232d574338260cc',
);

$request->setHeader($headers);

$parameters = array(
    // Request parameters
    'size' => '512',
);

$url->setQueryVariables($parameters);

$request->setMethod(HTTP_Request2::METHOD_GET);

// Request body
$request->setBody("{body}");

try
{
    //print_r($request);
    $response = @$request->send();
    echo $response->getBody();
}
catch (HttpException $ex)
{
    echo $ex;
}

?>