<?php
require("reach_globals.php");

//echo $apiKey;
//$objMeta = getGameMetadata($apiKey);
$objStats = new reachStats();
$objMeta = "http://www.bungie.net/api/reach/reachapijson.svc/game/metadata/".$objStats->apiKey;
$output = file_get_contents($objMeta);
$obj = json_decode($output);

//print_r($obj);

foreach($obj->Data->AllCommendationsById as $key => $arrValues)
{
	//echo "<br>$key";
	$arrInsert = $arrValues->Value;
	$strSQL = "replace into commendation values 
				(".$arrInsert->Id.",
					'".addslashes($arrInsert->Description)."',
					'".addslashes($arrInsert->Name)."',
					".$arrInsert->Iron.",
					".$arrInsert->Bronze.",
					".$arrInsert->Silver.",
					".$arrInsert->Gold.",
					".$arrInsert->Onyx.",
					".$arrInsert->Max."					
				)";	
	mysql_query($strSQL);
}

foreach($obj->Data->AllEnemiesById as $key => $arrValues)
{
	//echo "<br>$key";
	$arrInsert = $arrValues->Value;
	$strSQL = "replace into enemy values 
				(".$arrInsert->Id.",
					'".addslashes($arrInsert->Name)."',
					'".addslashes($arrInsert->Description)."',
					'".addslashes($arrInsert->ImageName)."'								
				)";	
	mysql_query($strSQL);
}

foreach($obj->Data->AllMapsById as $key => $arrValues)
{
	//echo "<br>$key";
	$arrInsert = $arrValues->Value;
	$strSQL = "replace into map values 
				(".$arrInsert->Id.",
					'".addslashes($arrInsert->Name)."',
					'".addslashes($arrInsert->ImageName)."',
					'".addslashes($arrInsert->MapType)."'								
				)";	
	mysql_query($strSQL);
}
foreach($obj->Data->AllMedalsById as $key => $arrValues)
{
	//echo "<br>$key";
	$arrInsert = $arrValues->Value;
	$strSQL = "replace into medal values 
				(".$arrInsert->Id.",
					'".addslashes($arrInsert->Name)."',
					'".addslashes($arrInsert->ImageName)."',
					'".addslashes($arrInsert->Description)."'								
				)";	
	mysql_query($strSQL);
}

foreach($obj->Data->AllWeaponsById as $key => $arrValues)
{
	//echo "<br>$key";
	$arrInsert = $arrValues->Value;
	$strSQL = "replace into weapon values 
				(".$arrInsert->Id.",
					'".addslashes($arrInsert->Description)."',					
					'".addslashes($arrInsert->Name)."'								
				)";	
	mysql_query($strSQL);
}

foreach($obj->Data->GameVariantClassesKeysAndValues as $key => $arrValues)
{		
	$strSQL = "replace into variant values 
				(".$arrValues->Value.",										
					'".addslashes($arrValues->Key)."'								
				)";	
	mysql_query($strSQL);
}

foreach($obj->Data->GlobalRanksById as $key => $arrValues)
{	
	$strSQL = "replace into rank values 
				('".$arrValues->Key."',										
					'".addslashes($arrValues->Value)."'								
				)";	
	mysql_query($strSQL);
}

echo "DONE!";
?>
