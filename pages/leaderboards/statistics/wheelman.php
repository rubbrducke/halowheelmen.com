<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<?php

include_once("groupClass.php");
include_once("statClass.php");
include_once("../globals.php");

isset($_GET["type"]) ? $type = $_GET["type"] : $type = "ranked";
isset($_GET["sort"]) ? $sort = $_GET["sort"] : $sort = "aveWheelman";

$group = new h3GroupStats();

if($type == "ranked" || $type == "rankedAll")
{
	switch ($sort)
	{
		case "wheelman":
			$list = $group->sortList("Wheelman","ranked");
			break;

		case "splatter":
			$list = $group->sortList("WarthogDriver","ranked");
			break;

		case "aveSplat":
			$list = $group->sortList("aveWarthogDriver","ranked");
			break;
		case "aveWheelman":
			$list = $group->sortList("aveWheelman","ranked");
			break;
		default:
			break;
	}

}
elseif ($type=="social" || $type == "socialAll")
{
	switch ($sort)
	{
		case "wheelman":
			$list = $group->sortList("Wheelman","social");
			break;

		case "splatter":
			$list = $group->sortList("WarthogDriver","social");
			break;

		case "aveSplat":
			$list = $group->sortList("aveWarthogDriver","social");
			break;
		case "aveWheelman":
			$list = $group->sortList("aveWheelman","social");
			break;
		default:
			break;
	}
}



if($type == "socialAll" || $type == "rankedAll")
{
	$count = sizeof($list);

}
else
{
	$count = 20;
}
?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
<title>Untitled 1</title>
<base target="_self" />

<style type="text/css">
A:link {
	COLOR: #ffffff; TEXT-DECORATION: underline
}
A:visited {
	COLOR: #ffffff; TEXT-DECORATION: underline
}
A:hover {
	COLOR: #b3b3b3; TEXT-DECORATION: none
}
A:active {
	COLOR: #ffffff; TEXT-DECORATION: underline
}

.style1 {
	text-align: center;
	background-color: #424345;
	font-family: Arial;
	color: #FFFFFF;
}
.style2 {
	color:#FFFFFF;
	background-color: #2e2f31;
}
.style3 {
	font-family: Arial;
	color:#FFFFFF;
	background-color: #2e2f31;
}
.style4 {
	font-family: Arial;
	text-align: center;
	color:#FFFFFF;
	background-color: #2e2f31;
}
.style5 {
	font-family: Arial;
	color:#FFFFFF;
	background-color: #424345;
}
.style6 {
	font-family: Arial;
	color:#FFFFFF;
	background-color: #424345;
}
</style>
</head>

<body style="background-color: #2e2f31; margin-top: 5px;">

<table class="style2" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td class="style1" colspan="6">Top <? echo ucfirst(substr($type,0,6))?> Wheelman</td>
	</tr>
	<tr>
		<td style="width: 21px" class="style3">&nbsp;</td>
		<td style="width: 201px" class="style3">Gamertag</td>
		<td style="width: 107px" class="style4" colspan="2">Wheelman</td>
		<td style="width: 133px" class="style4" colspan="2">Hog Splatters</td>
	</tr>
	<tr>
		<td style="width: 21px" class="style3">&nbsp;</td>
		<td style="width: 201px" class="style3">&nbsp;
		</td>
		<td style="width: 107px" class="style3"><a href="wheelman.php?type=<? echo $type?>&sort=wheelman">Total</a></td>
		<td style="width: 107px" class="style3"><a href="wheelman.php?type=<? echo $type?>&sort=aveWheelman">Ave</a></td>
		<td style="width: 133px" class="style3"><a href="wheelman.php?type=<? echo $type?>&sort=splatter">Total</a></td>
		<td style="width: 133px" class="style3"><a href="wheelman.php?type=<? echo $type?>&sort=aveSplat">Ave</a></td>
	</tr>
		<?
		$i=0;
		foreach ($list as $gamertag => $value)
		{
			if($i>$count-1)
			break;

			$result = mysql_query("Select * from stats where gamertag = '$gamertag'");
			if($result)
			{
				$row = mysql_fetch_array($result);
				echo "<!--Start of $gamertag-->";
				$user = unserialize($row["stats"]);
	
				//$user = new h3Stat($gamertag);
				$rWheelman = $user->rWheelman;
				$rSplatter = $user->rWarthogDriver;
				$sWheelman = $user->sWheelman;
				$sSplatter = $user->sWarthogDriver;
				$rGames = $user->rWheelmanGames;
				$sGames = $user->sWheelmanGames;
	
				if(substr($type,0,6)=="ranked")
				{
					$rGames == 0 ? $spave = 0 : $spave = $rSplatter/$rGames;
					$rGames == 0 ? $wave = 0 : $wave = $rWheelman/$rGames;
				}
				else
				{
					$sGames == 0 ? $spave = 0 : $spave = $sSplatter/$sGames;
					$sGames == 0 ? $wave = 0 : $wave = $sWheelman/$sGames;
					
				}
				if($i%2==1)
				$class = 3;
				else
				$class = 6;

    ?>

	<tr>
		<td style="width: 21px" class="style<? echo $class?>"><? echo $i+1?></td>
		<td style="width: 201px" class="style<? echo $class?>">
		<?
		if(substr($type,0,6)=="ranked")
		{?>
		<a target="_blank" href="http://www.bungie.net/Stats/Halo3/CareerStats.aspx?player=<?echo $gamertag?>&social=False"><?echo $gamertag?></a>
		<?}
		else
		{
		?>
		<a target="_blank" href="http://www.bungie.net/Stats/Halo3/CareerStats.aspx?player=<?echo $gamertag?>&social=true"><?echo $gamertag?></a>

		<?}?>
		</td>
		<td style="width: 107px" class="style<? echo $class?>">
		<?
		if(substr($type,0,6)=="ranked")
		{
			echo $rWheelman;
		}
		else
		{
			echo $sWheelman;
		}
		?>
			</td>
		<td style="width: 107px" class="style<? echo $class?>"><?echo round($wave,2)?></td>
		<td style="width: 133px" class="style<? echo $class?>">
		<?
		if(substr($type,0,6)=="ranked")
		echo $rSplatter;
		else
			echo $sSplatter;?>
			</td>
		<td style="width: 133px" class="style<? echo $class?>"><?echo round($spave,2)?></td>
	</tr>
	<?
	$i++;
	echo "<!--End of $gamertag-->";
	}
	
	
}//end foreach

		?>
	<tr>
		<td style="width: 21px" class="style3">&nbsp;</td>
		<td style="width: 201px" class="style3">&nbsp;</td>
		<td style="width: 107px" class="style3" colspan="2">&nbsp;</td>
		<td style="width: 133px" class="style3" colspan="2">
		<?
		if($type <> "rankedAll" and $type <> "socialAll")
		{
			?>
		<a href="wheelman.php?type=<? echo $type?>All&sort=<? echo $sort?>">See all --&gt;</a><?}?>&nbsp;</td>
	</tr>
</table>

<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-3489741-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>
<?
mysql_free_result($result);
mysql_close();
?>
</body>

</html>
